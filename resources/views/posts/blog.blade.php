@extends('layouts.app')

@section('title', 'Our Thinking - Blog | Aya Data')
@section('meta_description', 'Discover the Competitive Advantages of West Africas Data Professionals')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')

<section class="single-page-header" style="background-image: url({{ asset('img/cr-hero.jpg') }})" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="">Blog</h1>
                <p class="">Discover the Competitive Advantages of West Africa's Data Professionals</p>

                @if(Session::get('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
			</div>
		</div>
	</div>
</section>

<section class="blog" id="blog">
    <div class="container">
        <div class="row">
            @if( count($posts) > 0)
                @foreach($posts as $blogpost)
                <article class="col-md-4 col-sm-6 col-xs-12 clearfix ">
                    <div class="post-item">
                        <div class="media-wrapper">
                            <img src="{{ asset('img/blog/'.$blogpost->image) }}" alt="{{ $blogpost->title }}" class="img-fluid">
                        </div>

                        <div class="content">
                            <h5><a href="{{ route('blogsingle', $blogpost->slug) }}">{{ $blogpost->title }}</a></h5>
                            <p>{{ $blogpost->summary }}</p>
                            <p><small>Published on {{ $blogpost->created_at->format('j F, Y') }}</small></p>
                            {{-- <p><small>By {{ $blogpost->author->name }}</small></p> --}}
                            <a class="btn btn-main" href="{{ route('blogsingle', $blogpost->slug) }}">Read more</a>
                        </div>
                    </div>
                </article>
                @endforeach
            @else
                <p>No Posts Found!</p>
            @endif

            <!-- end single blog post -->
        </div> <!-- end row -->
    </div> <!-- end container -->
</section> <!-- end section -->

@endsection
