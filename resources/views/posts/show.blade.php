@extends('layouts.app')

@section('title', 'Our Thinking - Blog | Aya Data')
@section('meta_description', '12 years of building a successful enterprises in West Africa,</span> we recognised too well the gap between the abundance of talent and the sheer lack of good jobs.')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')


<section class="single-page-header" style="background-image: url({{ asset('img/cr-hero.jpg') }})" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>{{ $post->title }}</h2>
                {{-- <p>By: {{ $post->author->name }}</p> --}}
                <p>Published on: {{ $post->created_at->format('j F, Y') }}</p>
                <a href="{{ route('our-blog')}}" class="btn btn-main">Back to Our Blog</a>
			</div>
		</div>
	</div>
</section>

<!-- blog details part start -->
<section class="blog-details section">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 offset-md-2">
        <article class="post">
          <div class="post-image">
            <img class="img-fluid w-100 mb-3" src="{{ asset('img/blog/'.$post->image) }}" alt="post-image">
          </div>
          <!-- Post Content -->
          <div class="post-content">
            <p>{!! $post->body !!}</p>
          </div>
        </article>
      </div>
    </div>
  </div>
</section>
<!-- blog details part end -->

@endsection
