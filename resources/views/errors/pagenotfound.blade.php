<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>404! Error Page - Aya Data</title>
    <meta name="description" content="Power up your Machine Learning initiatives with Aya Data.">
    <meta name="keywords" content="">
    <meta name="author" content="RMQ Creative">
    <meta name="dcterms.rights" content="All Rights Reserved. AYA DATA">
    <meta name="copyright" content="All Rights Reserved. AYA DATA" />
    <meta name="application-name" content="Aya Data" />
    <meta name="robots" content="no-follow, no-index">

    <!-- Site Base -->
    {{-- <base href="http://localhost:8000"> --}}

<!-- CSS
  ================================================== -->
  <!-- Themefisher Icon font -->
  <link rel="stylesheet" href="{{asset('plugins/themefisher-font/style.css')}}">
  <!-- bootstrap.min css -->
  <link rel="stylesheet" href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}">
  <!-- Lightbox.min css -->
  <link rel="stylesheet" href="{{asset('plugins/lightbox2/dist/css/lightbox.min.css')}}">
  <!-- animation css -->
  <link rel="stylesheet" href="{{asset('plugins/animate/animate.css')}}">
  <!-- Slick Carousel -->
  <link rel="stylesheet" href="{{asset('plugins/slick/slick.css')}}">
  <!-- Main Stylesheet -->
  <link rel="stylesheet" href="{{asset('css/style.css')}}">

  <!--favicon icon-->
    <link rel="icon" href="{{ asset('img/favicon.png') }}" type="image/png" sizes="16x16" />

</head>

<body id="body">

    <!--
  Start Preloader
  ==================================== -->
  <div id="preloader">
    <div class='preloader'>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>

    <section class="page-404 section text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>404</h1>
					<h2>Page Not Found</h2>
  					<p>Sorry, but the page you were trying to view does not exist.</p>
					<a href="/" class="btn btn-main mt-20">Go Home</a>
				</div>
			</div>
		</div>
	</section>


<!--
    Essential Scripts
    =====================================-->
    <!-- Main jQuery -->
    <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
    <!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu5nZKbeK-WHQ70oqOWo-_4VmwOwKP9YQ"></script>
    <script  src="{{asset('plugins/google-map/gmap.js')}}"></script>

    <!-- Form Validation -->
    <script src="{{asset('plugins/form-validation/jquery.form.js')}}"></script>
    <script src="{{asset('plugins/form-validation/jquery.validate.min.js')}}"></script>

    <!-- Bootstrap4 -->
    <script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- Parallax -->
    <script src="{{asset('plugins/parallax/jquery.parallax-1.1.3.js')}}"></script>
    <!-- lightbox -->
    <script src="{{asset('plugins/lightbox2/dist/js/lightbox.min.js')}}"></script>
    <!-- Owl Carousel -->
    <script src="{{asset('plugins/slick/slick.min.js')}}"></script>
    <!-- filter -->
    <script src="{{asset('plugins/filterizr/jquery.filterizr.min.js')}}"></script>
    <!-- Smooth Scroll js -->
    <script src="{{asset('plugins/smooth-scroll/smooth-scroll.min.js')}}"></script>
    {{-- fontawersome --}}
    <script src="{{asset('js/all.min.js')}}"></script>

    <!-- Custom js -->
    <script src="{{asset('js/script.js')}}"></script>
</body>

</html>