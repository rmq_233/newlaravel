@extends('layouts.app')

@section('title', 'Our Services | Aya Data')
@section('meta_description', 'Power Up Your Machine Learning Initiatives with Aya Data.')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')

<section class="single-page-header" style="background-image: url({{ asset('img/ser-hero.jpg')}})" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="">Our <span class="text-dark">Services</span></h1>
                <p class="">Let us create the data sets to optimize your models, so you don’t have to.</p>
			</div>
		</div>
	</div>
</section>

<section class="service-2 section">
    <div class="container pt-5">
        <div class="row">

            <!-- single blog post -->
            <article class="col-md-4 col-sm-6 col-xs-12 clearfix ">
                <div class="post-item">
                    <div class="media-wrapper">
                        <img src="{{asset('img/services/cv.png')}}" alt="amazing caves coverimage" class="img-fluid">
                    </div>

                    <div class="content text-center">
                        <h5><a href="{{ route('services.computer-vision') }}">Computer Vision</a></h5>
                        <p>Annotate video, image, SAR and LIDAR based data sets using up-to-the-minute techniques and categorisations.</p>
                        <a class="btn btn-main" href="{{ route('services.computer-vision') }}">Read more</a>
                    </div>
                </div>
            </article>
            <!-- /single blog post -->

            <!-- single blog post -->
            <article class="col-md-4 col-sm-6 col-xs-12 ">
                <div class="post-item">
                    <div class="media-wrapper">
                        <img src="{{asset('img/services/nl.png')}}" alt="" class="img-fluid">
                    </div>

                    <div class="content text-center">
                        <h5><a href="{{ route('services.natural-language') }}">Natural Language Processing</a></h5>
                        <p>Optimize your AI’s understanding of sentiment, semantic meaning and language with English and French text etc</p>
                        <a class="btn btn-main" href="{{ route('services.natural-language') }}">Read more</a>
                    </div>
                </div>
            </article>
            <!-- end single blog post -->

            <!-- single blog post -->
            <article class="col-md-4 col-sm-6 col-xs-12 ">
                <div class="post-item">
                    <div class="media-wrapper">
                        <img src="{{asset('img/services/dp.png')}}" alt="amazing caves coverimage" class="img-fluid">
                    </div>

                    <div class="content text-center">
                        <h5><a href="{{route('services.data-processing')}}">Data Processing</a></h5>
                        <p>Enhance your data processing results with formatting, arrangement, visualisation and de-duplication tools.</p>
                        <a class="btn btn-main" href="{{route('services.data-processing')}}">Read more</a>
                    </div>
                </div>
            </article>
            <!-- end single blog post -->
        </div> <!-- end row -->
    </div> <!-- End container -->
</section> <!-- End section -->

<section class="about-2 section" id="about">
        <div class="container">
            <div class="row">

                <!-- section title -->
                <div class="col-12">
                    <div class="title text-center">
                        <h3>Our Work Process</h3>
                        <p class="lead">Our five-step engagement process prioritizes collaboration and communication as the means of achieving the best outcomes fast. Together, we discuss your objectives and tailor our solutions to your requirements, iterating wherever necessary.</p>
                        <div class="border"></div>
                    </div>
                </div>
                <!-- /section title -->

                {{-- video tag --}}
                <div class="col-md-6" hidden>
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/FW6dpEnf3hI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div> <!-- End row -->
        </div>
        <div class="timeline">
            <ul>
                <li>
                    <div>
                        <h1><span class="badge badge-warning">1</span></h1>
                        <h5>Talk To Our Experts</h5>
                        <p>
                            Our Solutions Experts help you understand where we can add the most value
                        </p>
                    </div>
                </li>
                <li>
                    <div class="text-right">
                        <h1><span class="badge badge-warning">2</span></h1>
                        <h5>Proof of Concept</h5>
                        <p>
                            Experience our work up close. Securely share an anonymised sample of your data, and we'll complete a complimentary proof of concept project.
                        </p>
                    </div>
                </li>
                <li>
                    <div>
                        <h1><span class="badge badge-warning">3</span></h1>
                        <h5>OnBoarding</h5>
                        <p>
                            Using improved onboarding tools, we manage the crucial user stages of resource allocation, upskilling and domain expertise transfer.
                        </p>
                    </div>
                </li>
                <li>
                    <div class="text-right">
                        <h1><span class="badge badge-warning">4</span></h1>
                        <h5>Launch and Iteration</h5>
                        <p>
                            We organise workflow iterations through a real-time feedback cycle between the client and delivery teams.
                        </p>
                    </div>
                </li>
                <li>
                    <div>
                        <h1><span class="badge badge-warning">5</span></h1>
                        <h5>Review</h5>
                        <p>
                            With you, we review project outcomes, performance against predetermined KPIs and provide insights into the next stage of your initiative.
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    </section> <!-- End section -->

<section class="team-skills section-sm" id="skills">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h1 class="display-3 p-5">Trusted <br><span class="text-primary">Partners</span></h1>
                </div>
                <div class="col-md-8">
                    <div id="clients-slider" class="clients-logo-slider">
                        <img src="{{ asset('img/client-logo/p1.png') }}" class="img-fluid" alt="Data Loop">
                        <img src="{{ asset('img/client-logo/p2.png') }}" class="img-fluid" alt="Labelbox">
                        <img src="{{ asset('img/client-logo/p3.png') }}" class="img-fluid" alt="Demeter Ghana Ltd.">
                        <img src="{{ asset('img/client-logo/p4.png') }}" class="img-fluid" alt="eQuality Talent">
                        <img src="{{ asset('img/client-logo/p5.png') }}" class="img-fluid" alt="OpenLabs NIIT Ghana">
                    </div>

                </div>
            </div>
            <div class="p-2"></div>
            <div class="row">
                <div class="col-md-6">
                    <img src="https://dummyimage.com/1000x500/000/fff" class="img-fluid" alt="Machine Learning - Ayadata">
                </div>
                <div class="col-md-6">
                    <div class="team-skills-content">
                        <h2>Let's Turn Your Machine Learning Objectives Into Realty</h2>
                        <p class="lead">Effective machine learning requires the best quality data services. Discover how Aya Data’s expertise can meet your targets and deliver on value, flexibility, security and cost.</p>
                        <a href="{{ route('contact-us') }}" class="btn btn-main">Contact Us</a>
                    </div>
                </div>
            </div> <!-- End row -->
        </div> <!-- End container -->
    </section> <!-- End section -->

@endsection
