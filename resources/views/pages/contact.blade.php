@extends('layouts.app')

@section('title', 'Contact Us | Aya Data')
@section('meta_description', 'Discover the Competitive Advantages of West Africa.s Data Professionals')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')

<section class="single-page-header" style="background-image: url({{ asset('img/contact-hero.png')}})" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="">Contact <span class="text-danger">Us</span></h1>
                <p class="">Discover the Competitive Advantages of West Africa's Data Professionals</p>
			</div>
		</div>
	</div>
</section>

<section class="contact-us" id="contact">
	<div class="container">
		<div class="row">
			<!-- Contact Details -->
			<div class="contact-details col-md-6 " >
				<h3>Aya Data Headquarters</h3>
				<ul class="contact-short-info" >
					<li>
						<i class="tf-ion-ios-home"></i>
						<span>15-19 Bloomsbury Way , Holborn, London, WC1A 2TH</span>
					</li>
					<li>
						<i class="tf-ion-android-phone-portrait"></i>
						<span><a href="tel:+44-33-377-21194">Phone: +44-33-377-21194</a></span>
					</li>
					<li>
						<i class="tf-ion-android-mail"></i>
						<span><a href="mailto:info@ayadata.ai">Email: info@ayadata.ai</a></span>
					</li>
				</ul>

                <ul class="contact-short-info" >
                    <li><b>Ghana Office Address</b></li>
					<li>
						<i class="tf-ion-ios-home"></i>
						<span>Aya Data Ltd., 87 Spintex Road, Accra - Ghana</span>
					</li>
				</ul>

                    {{-- hat --}}
				<!-- Footer Social Links -->
				<div class="social-icon">
					<ul>
                        <li><a href="https://www.facebook.com/Ayadatagh/"><i class="tf-ion-social-facebook"></i></a></li>
                        <li><a href="https://twitter.com/ayadatagh"><i class="tf-ion-social-twitter"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/aya-data"><i class="tf-ion-social-linkedin-outline"></i></a></li>
                    </ul>
				</div>
				<!--/. End Footer Social Links -->
			</div>
			<!-- / End Contact Details -->

			<!-- Contact Form -->
			<div class="contact-form col-md-6 " >
				<form method="post" role="form" action="{{ route('send.email') }}">
                    @csrf

                    @if (Session::has('error'))
                        <div class="alert alert-danger mb-1 mt-1">
                            {{ Session::get('error') }}
                        </div>
                    @endif

                    @if (Session::has('success'))
                        <div class="alert alert-success mb-1 mt-1">
                            {{ Session::get('success') }}
                        </div>
                    @endif


					<div class="form-group">
						<input type="text" placeholder="Your Name" class="form-control" name="name" id="name" value="{{ old('name') }}">
						@error('name')
						    <div class="alert alert-danger mt-1">{{ $message }}</div>
						@enderror
					</div>

					<div class="form-group">
						<input type="email" placeholder="Your Email" class="form-control" name="email" id="email" value="{{ old('email') }}">
						@error('email')
						    <div class="alert alert-danger mt-1">{{ $message }}</div>
						@enderror
					</div>

					<div class="form-group">
						<input type="text" placeholder="Subject" class="form-control" name="subject" id="subject" value="{{ old('subject') }}">
						@error('subject')
						    <div class="alert alert-danger mt-1">{{ $message }}</div>
						@enderror
					</div>

					<div class="form-group">
						<textarea rows="6" placeholder="Message" class="form-control" name="message" id="message">{{ old('message') }}</textarea>
						@error('message')
						    <div class="alert alert-danger mt-1">{{ $message }}</div>
						@enderror
					</div>
					<div id="cf-submit">
						<button type="submit" id="contact-submit" class="btn btn-transparent" value="Submit">Submit</button>
					</div>
                </form>
			</div>
			<!-- ./End Contact Form -->
		</div> <!-- end row -->
	</div> <!-- end container -->
</section> <!-- end section -->

<!--================================
=            Google Map            =
=================================-->
<section class="map">
	<!-- Google Map -->
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.753723809466!2d-0.1255094839500145!3d51.51773407963682!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761b347f81da17%3A0xc699b8c24c80ee37!2s15-19%20Bloomsbury%20Way%2C%20London%20WC1A%202TH%2C%20UK!5e0!3m2!1sen!2sgh!4v1634478221853!5m2!1sen!2sgh" width="100%" height="350" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</section>
<!--====  End of Google Map  ====-->

@endsection
