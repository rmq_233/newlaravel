@extends('layouts.app')

@section('title', 'Data Processing - Our Services | Aya Data')
@section('meta_description', 'Power Up Your Machine Learning Initiatives with Aya Data.')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')

<section class="single-page-header" style="background-image: url({{ asset('img/data-hero.png')}})" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="">Data <span class="text-warning">Processing</span></h1>
                <p class="">Enhance your data sets with Formatting, Arrangement, Visualisation and De-duplication</p>
			</div>
		</div>
	</div>
</section>

<section class="about-2 section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{ asset('../img/dataprocessing.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-md-6">
                    <ul class="checklist">
                        <li>Data is only as valuable as the insight that it allows you to discover.</li>
                        <li>To get the most out of your data, it is essential to keep it well managed and clean, and to understand how to interpret it.</li>
                        <li>Our Data Specialists are experts in cleansing, manipulating and visualising large data sets, using tools like SQL, Alteryx and PowerBI.</li>
                        <li>Book a free consultation to find out how we can maximise the value of your data.</li>
                    </ul>
                </div>
            </div> <!-- End row -->
        </div> <!-- End container -->
    </section>


@endsection
