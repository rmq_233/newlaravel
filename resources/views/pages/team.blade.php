@extends('layouts.app')

@section('title', 'Our Team - About Us | Aya Data')
@section('meta_description', 'Aya is the Adinkra word for resourcefulness. True to our name we have aimed to create a haven for resourceful people, regardless of social or economic background.')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')

<section class="single-page-header" style="background-image: url('../img/team.jpg')" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="">Our Team</h1>
                <p class="text-">The engine room behind the ideas.</p>
			</div>
		</div>
	</div>
</section>

<section class="about" id="about">
	<div class="container">
		<div class="row">

			<div class="col-md-6">
				<img src="https://dummyimage.com/1000x800/000/fff" class="img-fluid" alt="Aya Data Team Picture">
			</div>
			<div class="col-md-6">
				<h2 class="">Resourcefulness is our name, <br><span class="text-info">Diversity is our strength.</span></h2>
				<p class="lead underyou">Aya is the Adinkra word for resourcefulness. True to our name we have aimed to create a haven for resourceful people, regardless of social or economic background.</p>
				<p class="lead mt-3">That is why in our office you will find people with no academic qualifications working side by side with graduates from the best Universities in West Africa. It is also why you might hear over 20 languages spoken in our corridors.</p>
                <p class="lead">Diversity is crucial in an effective team, but so is cohesion - the thread that binds us together is resourcefulness: the ability to find clever ways to overcome difficult problems.</p>
			</div>
		</div> 		<!-- End row -->
	</div>   	<!-- End container -->
</section>   <!-- End section -->

<section  class="counter-wrapper section-sm" >
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center mb-5">
                <div class="title">
                    <h2>Find out how we can solve your problems</h2>
                    <p class="lead"></p>
                </div>
                <a data-duration-in=".3" data-animation-in="fadeInDown" data-delay-in=".8"  class="btn btn-main" href="{{ route('contact-us') }}">Contact Us</a>
            </div>
        </div><!-- end row -->
    </div>   	<!-- end container -->
</section>

<section class="team" id="team">
	<div class="container">
		<div class="row">

			<!-- section title -->
			<div class="col-12">
				<div class="title text-center ">
					<h2>Our Team</h2>
					<div class="border"></div>
				</div>
			</div>
			<!-- /section title -->

			<div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/als.jpg') }}" alt="Ama Larbi-Siaw">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/ama-ls-671178b2/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Ama Larbi-Siaw</h3>
						<span>Co-Founder</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/fm.jpg') }}" alt="Freddie Monk">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/freddie-monk-38347456/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Freddie Monk</h3>
						<span>Co-Founder</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/jah.jpg') }}" alt="Jesslin Anna Hammond">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/jesslin-anna-efua-hammond-5b51b7128/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Jesslin Anna Hammond</h3>
						<span>Human Resource Manager</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/ead.jpg') }}" alt="Edward Agyeman-Duah">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/edward-agyemang-duah-6b4a6090/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Edward Agyeman-Duah</h3>
						<span>Team Lead</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/sy.jpg') }}" alt="Stephen Yiadom">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/stephen-owusu/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Stephen Yiadom</h3>
						<span>Technology Lead</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/po.jpg') }}" alt="Prince Oberko">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/prince-kwesi-oberko-3620073b/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Prince Oberko</h3>
						<span>Snr. Data Protection Officer</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/skl.jpg') }}" alt="Selorm Kosi Lodonu">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/selorm-lodonu-a00263148/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Selorm Kosi Lodonu</h3>
						<span>Data Protection Officer</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/cda.jpg') }}" alt="Charlotte Dawson-Amoah">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/charlotte-dawson-amoah-2019/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Charlotte Dawson-Amoah</h3>
						<span>Business Development Analyst</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/ey.jpg') }}" alt="Ebenezer Yanney">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/ebenezer-yeboah-yanney-115789132/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Ebenezer Yanney</h3>
						<span>Web Developer</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/ai.jpg') }}" alt="Adnan Inusah">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.credly.com/users/adnan-inusah/badges" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Adnan Inusah</h3>
						<span>Data Specialist</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/aa.jpg') }}" alt="Amanda Appianteng">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/amanda-appiateng-656183216/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Amanda Appianteng</h3>
						<span>Data Specialist</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/si.jpg') }}" alt="Sharon Iphy">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/sharon-iphy-227b97195/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Sharon Iphy</h3>
						<span>Data Specialist</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/jo.jpg') }}" alt="Jeanne Ocran">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/jeanne-ocran-887ab919a/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Jeanne Ocran</h3>
						<span>Data Specialist</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/lf.jpg') }}" alt="Lydia Frimpong">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/lydia-frimpong-6a5b62136/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Lydia Frimpong</h3>
						<span>Data Specialist</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/bd.jpg') }}" alt="Barbara Dankyi">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/barbara-dankyi-524205207/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Barbara Dankyi</h3>
						<span>Data Specialist</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/td.jpg') }}" alt="Timothy Debrah">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/timothy-charles-debrah-65781914a/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Timothy Debrah</h3>
						<span>Data Specialist</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/vy.jpg') }}" alt="Victoria Yirenkyi">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/akosua-yirenkyi-325069129/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Victoria Yirenkyi</h3>
						<span>Data Specialist</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/sm.jpg') }}" alt="Stephen Mensah">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/stephen-mensah-298266143/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Stephen Mensah</h3>
						<span>Data Specialist</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/fw.jpg') }}" alt="Faisal Wahabu">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/faisalwahabu/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Faisal Wahabu</h3>
						<span>Data Specialist</span>

					</div>
				</div>
			</div>

            <div class="col-md-3 col-sm-6 ">
				<div class="team-member text-center">
					<div class="member-photo">
						<img class="img-fluid" src="{{ asset('img/team/fc.jpg') }}" alt="Felicia Gokah">
						<div class="mask">
							<ul class="clearfix">
								<li><a href="https://www.linkedin.com/in/felicia-nelly-gokah-4b6416b8/" target="_blank"><i class="tf-ion-social-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
                    <div class="member-content">
						<h3>Felicia Gokah</h3>
						<span>Data Specialist</span>

					</div>
				</div>
			</div>

		</div> <!-- End row -->
	</div> <!-- End container -->
</section> <!-- End section -->

<section class="call-to-action-3 section text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <img class="img-fluid" src="{{ asset('img/call-to-action/workforce.png') }}" alt="our workforce" style="width: 40%; height: auto;"class="inline-block img-fluid">
                <h3>Our <span class="text-warning">Workforce</span> Strategy</h3>
                <h1 class="">Our mission is to find and develop resourceful people. With the help of our leading-edge L&D partners we are building West African futures in Tech.</h1>
                <a href="{{ route('contact-us') }}" class="btn btn-main">Join Us</a>
            </div>
        </div> 		<!-- End row -->
    </div>   	<!-- End container -->
</section>   <!-- End section -->
@endsection
