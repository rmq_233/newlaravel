@extends('layouts.app')

@section('title', 'About Us | Aya Data')
@section('meta_description', '12 years of building a successful enterprises in West Africa,</span> we recognised too well the gap between the abundance of talent and the sheer lack of good jobs.')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')

<section class="single-page-header" style="background-image: url({{ asset('img/abt-hero.jpg') }})" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="">About <span class="text-danger">Us</span></h1>
                <p class="">Discover the Competitive Advantages of West Africa's Data Professionals</p>
			</div>
		</div>
	</div>
</section>

<section class="about-shot-info section-sm">
	<div class="container">
		<div class="row">
			<div class="col-md-5 mt-20">
				<p class="lead">After <span class="">12 years of building a successful enterprises in West Africa,</span> we recognised too well the gap between the abundance of talent and the sheer lack of good jobs.</p>
                <p class="lead underyou">It takes a graduate from one of <b>Ghana's 65+ universities</b> an average of seven years to start a professional career.</p>
                <p class="lead mt-3">
                    In 2020, we founded our company to address this imbalance and created opportunity where talent already exists. We named ourselves Aya after the Adinkra symbol for resilience and endurance, a reflection of the values that have seen us thrive in a complex industry as well as our team who never give up on a challenge.
                </p>
			</div>
			<div class="col-md-7">
				<img class="img-fluid" src="https://dummyimage.com/1000x700/000/fff" alt="Aya Data - Data Structure">
			</div>
		</div>
	</div>
</section>

<section class="company-mission section-sm bg-gray text-center">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3 class="text-primary">Our Mission</h3>
				<p class="lead">Our mission is simple, deliver exceptional data annotation services to power machine learning initiatives globally, and create futures in technology in West Africa</p>
				{{-- <img src="https://picsum.photos/1000/500" alt="" class="img-fluid mt-30"> --}}
			</div>
			<div class="col-md-6">
				<h3 class="text-primary">Our Vision</h3>
				<p class="lead">Aya Data are already one of the largest training data providers in West Africa - our vision over the next five years is not only to create 10,000 jobs in the sector, but to build Africa's first globally renowned centre for AI excellence</p>
				{{-- <img src="https://picsum.photos/1000/500" alt="" class="img-fluid mt-30"> --}}
			</div>
		</div>
	</div>
</section>

<section class="call-to-action-3 section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2 text-center">
				<h3>Join us and unlock your potential as part of the team that is paving the way for AI in West Africa.</h3>
                <a data-duration-in=".3" data-animation-in="fadeInDown" data-delay-in=".8"  class="btn btn-main" href="{{ route('about.careers') }}">View Careers</a>
			</div>
		</div> 		<!-- End row -->
	</div>   	<!-- End container -->
</section>

<section  class="counter-wrapper section-sm" >
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center mb-5">
                <div class="title">
                    <h2>Find out more about the Aya <span class="text-danger">difference</span></h2>
                </div>
                <a data-duration-in=".3" data-animation-in="fadeInDown" data-delay-in=".8"  class="btn btn-main" href="{{ route('contact-us') }}">Contact Us</a>
            </div>
            <!-- first count item -->
            <div class="col-md-4 col-sm-6 col-xs-6 text-center " >
                <div class="counters-item">
                    <i class="tf-ion-ios-alarm-outline"></i>
                    <div>
                        <span class="counter" data-count="15345">0</span>
                    </div>
                    <h3>Hours Saved</h3>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-6 text-center " >
                <div class="counters-item">
                    <i class="tf-ion-ios-analytics-outline"></i>
                    <div>
                        <span class="counter" data-count="27">0</span>
                    </div>
                    <h3>Projects completed</h3>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-6 text-center "  >
                <div class="counters-item kill-border">
                    <i class="tf-ion-ios-compose-outline"></i>
                    <div>
                        <span class="counter" data-count="100"></span>
                        <span class="lead">%</span>
                    </div>
                    <h3>Positive feedback</h3>

                </div>
            </div>
        </div><!-- end row -->
    </div>   	<!-- end container -->
</section>

<section class="testimonial section" id="testimonial" hidden>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title text-center ">
                    <div class="media-wrapper">
                        <img src="{{asset('img/forum.png')}}" alt="Our Workforce - AyaData" class="img-fluid">
                    </div>
                    <h3 class="text-white">What Some of Our Clients Say...</h3>
                    <div class="border"></div>
                </div>
            </div>
            <div class="col-lg-12">
                <!-- testimonial wrapper -->
                <div class="testimonial-slider">
                    <!-- testimonial single -->
                    <div class="item text-center">
                        <!-- client info -->
                        <div class="client-details">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum nulla, soluta dolorum. Eos earum, magni asperiores, unde corporis labore, enim, voluptatum officiis voluptates alias natus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, officia. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, quia?</p>
                        </div>
                        <!-- /client info -->
                        <!-- client photo -->
                        <div class="client-thumb">
                            <img src="https://picsum.photos/100" class="img-fluid" alt="client name">
                        </div>
                        <div class="client-meta">
                            <h3>William Martin</h3>
                            <span>CEO , Company Name</span>
                        </div>
                        <!-- /client photo -->
                    </div>
                    <!-- /testimonial single -->

                    <!-- testimonial single -->
                    <div class="item text-center">
                        <!-- client info -->
                        <div class="client-details">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum nulla, soluta dolorum. Eos earum, magni asperiores, unde corporis labore, enim, voluptatum officiis voluptates alias natus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, officia. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, quia?</p>
                        </div>
                        <!-- /client info -->
                        <!-- client photo -->
                        <div class="client-thumb">
                            <img src="https://picsum.photos/100" class="img-fluid" alt="client name">
                        </div>
                        <div class="client-meta">
                            <h3>Emma Harrison</h3>
                            <span>CEO , Company Name</span>
                        </div>
                        <!-- /client photo -->
                    </div>
                    <!-- /testimonial single -->

                    <!-- testimonial single -->
                    <div class="item text-center">
                        <!-- client info -->
                        <div class="client-details">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum nulla, soluta dolorum. Eos earum, magni asperiores, unde corporis labore, enim, voluptatum officiis voluptates alias natus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, officia. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, quia?</p>
                        </div>
                        <!-- /client info -->
                        <!-- client photo -->
                        <div class="client-thumb">
                            <img src="https://picsum.photos/100" class="img-fluid" alt="client name">
                        </div>
                        <div class="client-meta">
                            <h3>Alexander Lucas</h3>
                            <span>CEO , Company Name</span>
                        </div>
                        <!-- /client photo -->
                    </div>
                    <!-- /testimonial single -->
                </div>
            </div> 		<!-- end col lg 12 -->
        </div>	    <!-- End row -->
    </div>       <!-- End container -->
</section>

@endsection
