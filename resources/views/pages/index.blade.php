@extends('layouts.app')

@section('title', 'Welcome | Aya Data')
@section('meta_description', 'Power Up Your Machine Learning Initiatives with Aya Data.')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')

    {{-- Welcome Banner --}}
    <section class="hero-area">
        <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <h5 class="">Aya <span class="text-primary typeIt">Endurance</span></h5>
                        <h4 class="" data-duration-in=".3" data-animation-in="fadeInUp" data-delay-in=".1"><span class="text-danger">High Quality AI Training Data</span> to Power Up your Machine Learning Initiatives</h4>
                        <p class="lead text-dark" data-duration-in=".3" data-animation-in="fadeInUp" data-delay-in=".5">Aya Data delivers bespoke data annotation services to create high quality AI training data sets for your Computer Vision and Natural Language Processing projects. Book a free consultation now to learn how we communicate, assure quality and speed, and deliver value.</p>
                        <a data-duration-in=".3" data-animation-in="fadeIn" data-delay-in=".8" class="btn btn-main" href="{{route('contact-us')}}">Book Your Free Consultation</a>
                    </div>
                </div>
            </div>
    </section>

    <section class="service-2 section">
        <div class="container">
            <div class="row">

                <!-- section title -->
                <div class="col-12">
                    <div class="title text-center ">
                        <h3>Discover <span class="color">Our Services</span></h3>
                        <div class="border"></div>
                    </div>
                </div>
                <!-- /section title -->
                <!-- single blog post -->
                <article class="col-md-4 col-sm-6 col-xs-12 clearfix ">
                    <div class="post-item">
                        <div class="media-wrapper">
                            <img src="{{asset('img/services/cv.png')}}" alt="Computer Vision" class="img-fluid">
                        </div>

                        <div class="content text-center">
                            <h5><a href="{{ route('services.computer-vision') }}">Computer Vision</a></h5>
                            <p>Annotate video, image, SAR and LIDAR based data sets using up-to-the-minute techniques and categorisations.</p>
                            <a class="btn btn-main" href="{{ route('services.computer-vision') }}">Read more</a>
                        </div>
                    </div>
                </article>
                <!-- /single blog post -->

                <!-- single blog post -->
                <article class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="post-item">
                        <div class="media-wrapper">
                            <img src="{{asset('img/services/nl.png')}}" alt="Natural Language Process" class="img-fluid">
                        </div>

                        <div class="content text-center">
                            <h5><a href="{{ route('services.natural-language') }}">Natural Language Processing</a></h5>
                            <p>Optimize your AI’s understanding of sentiment, semantic meaning and language with English and French text etc</p>
                            <a class="btn btn-main" href="{{ route('services.natural-language') }}">Read more</a>
                        </div>
                    </div>
                </article>
                <!-- end single blog post -->

                <!-- single blog post -->
                <article class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="post-item">
                        <div class="media-wrapper">
                            <img src="{{asset('img/services/dp.png')}}" alt="Data Processing" class="img-fluid">
                        </div>

                        <div class="content text-center">
                            <h5><a href="{{route('services.data-processing')}}">Data Processing</a></h5>
                            <p>Enhance your data processing results with formatting, arrangement, visualisation and de-duplication tools.</p>
                            <a class="btn btn-main" href="{{route('services.data-processing')}}">Read more</a>
                        </div>
                    </div>
                </article>
                <!-- end single blog post -->
            </div> <!-- end row -->
        </div> <!-- End container -->
    </section> <!-- End section -->

    <section class="call-to-action-3 section text-center" hidden>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="service-item">
                        <h5>Data Annotation Type 1</h5>
                        <p class="text-warning">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore odio distinctio.</p>
                    </div>
                </div><!-- END COL -->

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="service-item">
                        <h5>Data Annotation Type 2</h5>
                        <p class="text-warning">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore odio distinctio.</p>
                    </div>
                </div><!-- END COL -->

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="service-item">
                        <h5>Data Annotation Type 3</h5>
                        <p class="text-warning">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore odio distinctio.</p>
                    </div>
                </div><!-- END COL -->
            </div> 		<!-- End row -->
        </div>   	<!-- End container -->
    </section>   <!-- End section -->

    <section class="services" id="services">
        <div class="container">
            <div class="row no-gutters">
                <!-- section title -->
                <div class="col-12">
                    <div class="title text-center mt-5">
                        <h3>Why <span class="color">Aya Data</span></h3>
                        <div class="border"></div>
                    </div>
                </div>
                <!-- section title -->
                <div class="col-12">
                    <div class="title text-center">
                        <p class="lead">Our mission is twofold: <span class="text-primary">Create good jobs in emerging economies and deliver exceptional data labelling services.</span> As well as contributing to the advancement of AI globally, we are focussed on <span class="text-primary">developing the next generation of West African data experts.</span></p>
                        <br>
                        <p class="lead">To deliver the best data labelling service, we prioritise:</p>
                        <div class="border"></div>
                    </div>
                </div>
                <!-- /section title -->

                <!-- Single Service Item -->
                <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
                    <div class="service-block p-4 text-center">
                        <div class="service-icon text-center">
                            <img src="{{ asset('img/about/conversation.png') }}" class="inline-block img-fluid" alt="Communication">
                        </div>
                        <h5>Communication</h5>
                        <p>The only way for us to exceed your expectations is to understand them in real-time. We see consistent and effective comm's as a requirement to achieving the best outcomes.</p>
                    </div>
                </div>
                <!-- End Single Service Item -->

                <!-- Single Service Item -->
                <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
                    <div class="service-block p-4 color-bg text-center">
                        <div class="service-icon text-center">
                            <img src="{{ asset('img/about/sweep_profile.png') }}" class="inline-block img-fluid" alt="Flexibility">
                        </div>
                        <h5>Flexibility</h5>
                        <p>Our teams scale to match your demand. We are technology agnostic and will choose the best tech stack for<br> the job.</p>
                    </div>
                </div>
                <!-- End Single Service Item -->

                <!-- Single Service Item -->
                <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
                    <div class="service-block p-4 text-center">
                        <div class="service-icon text-center">
                            <img src="{{ asset('img/about/security_levels.png') }}" class="inline-block img-fluid" alt="Security">
                        </div>
                        <h5>Security</h5>
                        <p>We follow the highest standards of data security. We are GDPR and SOC 2 compliant.</p>
                    </div>
                </div>
                <!-- End Single Service Item -->

                <!-- Single Service Item -->
                <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
                    <div class="service-block p-4 color-bg text-center">
                        <div class="service-icon text-center">
                            <img src="{{ asset('img/about/budget.png') }}" class="inline-block img-fluid" alt="Cost Effectiveness">
                        </div>
                        <h5>Cost Effectiveness</h5>
                        <p>Competitively structured packages at transparent price<br> points</p>
                    </div>
                </div>
                <!-- End Single Service Item -->

                <!-- Single Service Item -->
                <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
                    <div class="service-block p-4 text-center">
                        <div class="service-icon text-center">
                            <img src="{{ asset('img/about/quality.png') }}" class="inline-block img-fluid" alt="Quality">
                        </div>
                        <h5>Quality</h5>
                        <p>Quality is not a standard metric, it's defined by you. Once KPIs are set, we iterate our workflow to deliver the exact results that you need.</p>
                    </div>
                </div>
                <!-- End Single Service Item -->

                <!-- Single Service Item -->
                <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
                    <div class="service-block p-4 color-bg text-center">
                        <div class="service-icon text-center">
                            <img src="{{ asset('img/about/target2.png') }}" class="inline-block img-fluid" alt="Impact">
                        </div>
                        <h5>Impact</h5>
                        <p>As well as stable career prospects, our staff have the option of free, ongoing technical training through our University Partners</p>
                    </div>
                </div>
                <!-- End Single Service Item -->
            </div> <!-- End row -->
        </div> <!-- End container -->
    </section> <!-- End section -->

    <section class="call-to-action-3 section text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <img class="img-fluid" src="{{ asset('img/call-to-action/workforce.png') }}" alt="Our Workforce" style="width: 40%; height: auto;"class="inline-block img-fluid">
                    <h3>Our <span class="text-warning">Workforce</span> Strategy</h3>
                    <h1 class="">Our mission is to find and develop resourceful people. With the help of our leading-edge L&D partners we are building West African futures in Tech.</h1>
                    <a href="{{ route('contact-us') }}" class="btn btn-main">Join Us</a>
                </div>
            </div> 		<!-- End row -->
        </div>   	<!-- End container -->
    </section>   <!-- End section -->


    <section class="about-2 section" id="about">
        <div class="container">
            <div class="row">

                <!-- section title -->
                <div class="col-12">
                    <div class="title text-center">
                        <h3>Our Work Process</h3>
                        <p class="lead">Our five-step engagement process prioritizes collaboration and communication as the means of achieving the best outcomes fast. Together, we discuss your objectives and tailor our solutions to your requirements, iterating wherever necessary.</p>
                        <div class="border"></div>
                    </div>
                </div>
                <!-- /section title -->

                {{-- video tag --}}
                <div class="col-md-6" hidden>
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/FW6dpEnf3hI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div> <!-- End row -->
        </div>
        <div class="timeline">
            <ul>
                <li>
                    <div>
                        <h1><span class="badge badge-warning">1</span></h1>
                        <h5>Talk To Our Experts</h5>
                        <p>
                            Our Solutions Experts help you understand where we can add the most value
                        </p>
                    </div>
                </li>
                <li>
                    <div class="text-right">
                        <h1><span class="badge badge-warning">2</span></h1>
                        <h5>Proof of Concept</h5>
                        <p>
                            Experience our work up close. Securely share an anonymised sample of your data, and we'll complete a complimentary proof of concept project.
                        </p>
                    </div>
                </li>
                <li>
                    <div>
                        <h1><span class="badge badge-warning">3</span></h1>
                        <h5>OnBoarding</h5>
                        <p>
                            Using improved onboarding tools, we manage the crucial user stages of resource allocation, upskilling and domain expertise transfer.
                        </p>
                    </div>
                </li>
                <li>
                    <div class="text-right">
                        <h1><span class="badge badge-warning">4</span></h1>
                        <h5>Launch and Iteration</h5>
                        <p>
                            We organise workflow iterations through a real-time feedback cycle between the client and delivery teams.
                        </p>
                    </div>
                </li>
                <li>
                    <div>
                        <h1><span class="badge badge-warning">5</span></h1>
                        <h5>Review</h5>
                        <p>
                            With you, we review project outcomes, performance against predetermined KPIs and provide insights into the next stage of your initiative.
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    </section> <!-- End section -->

    <section class="blog" id="blog">
        <div class="container">
            <div class="row">

                <!-- section title -->
                <div class="col-12">
                    <div class="title text-center ">
                        <h3> Our <span class="color">Thinking</span></h3>
                        <div class="border"></div>
                    </div>
                </div>
                <!-- /section title -->

                {{-- Each Blog --}}
                @if( count($posts) > 0)
                    @foreach($posts as $blogpost)
                    <article class="col-md-4 col-sm-6 col-xs-12 clearfix ">
                        <div class="post-item">
                            <div class="media-wrapper">
                                <img src="{{ asset('img/blog/'.$blogpost->image) }}" alt="{{ $blogpost->title }}" class="img-fluid">
                            </div>

                            <div class="content">
                                <h5><a href="{{ route('blogsingle', $blogpost->slug) }}">{{ $blogpost->title }}</a></h5>
                                <p>{{ $blogpost->summary }}</p>
                                <p><small>Published on {{ $blogpost->created_at->format('j F, Y') }}</small></p>
                                {{-- <p><small>By {{ $blogpost->author->name }}</small></p> --}}
                                <a class="btn btn-main" href="{{ route('blogsingle', $blogpost->slug) }}">Read more</a>
                            </div>
                        </div>
                    </article>
                    @endforeach
                @else
                    <p>No Posts Found!</p>
                @endif

            </div> <!-- end row -->
        </div> <!-- end container -->
    </section> <!-- end section -->

    <section class="team-skills section-sm" id="skills">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h1 class="display-3 p-5">Trusted <br><span class="text-primary">Partners</span></h1>
                </div>
                <div class="col-md-8">
                    <div id="clients-slider" class="clients-logo-slider">
                        <img src="{{ asset('img/client-logo/p1.png') }}" class="img-fluid" alt="Data Loop">
                        <img src="{{ asset('img/client-logo/p2.png') }}" class="img-fluid" alt="Labelbox">
                        <img src="{{ asset('img/client-logo/p3.png') }}" class="img-fluid" alt="Demeter Ghana Ltd.">
                        <img src="{{ asset('img/client-logo/p4.png') }}" class="img-fluid" alt="eQuality Talent">
                        <img src="{{ asset('img/client-logo/p5.png') }}" class="img-fluid" alt="OpenLabs NIIT Ghana">
                    </div>

                </div>
            </div>
            <div class="p-2"></div>
            <div class="row">
                <div class="col-md-6">
                    <img src="https://dummyimage.com/1000x500/000/fff" class="img-fluid" alt="Machine Learning - Ayadata">
                </div>
                <div class="col-md-6">
                    <div class="team-skills-content">
                        <h2>Let's Turn Your Machine Learning Objectives Into Realty</h2>
                        <p class="lead">Effective machine learning requires the best quality data services. Discover how Aya Data’s expertise can meet your targets and deliver on value, flexibility, security and cost.</p>
                        <a href="{{ route('contact-us') }}" class="btn btn-main">Contact Us</a>
                    </div>
                </div>
            </div> <!-- End row -->
        </div> <!-- End container -->
    </section> <!-- End section -->

    {{-- modal --}}
    <div id="infoModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <h5 class="modal-title text-capitalize">Get a high quality training data set, for free!</h5>
                    <img class="img-fluid p-2" src="{{ asset('img/about/data.png') }}" alt="Aya Data - Data Structure">
                    <p class="lead underyou">In our opinion, data structuring is a service best founded on trust and communication. That's why if you're new to us, we'll deliver a Proof of Value project for you, for free</p>
                    <p class="lead">Securely send us an unstructured data-set of your choice, we'll give you up to 48 hours of our time to annotate it according to your exact requirements!</p>
                    <h5>No fee. No strings attached.</h5>
                    <a href="{{ route('contact-us') }}" class="btn btn-main-alt">Book Your Free Consultation</a>
                    {{-- <a href="{{ route('contact-us') }}" class="btn btn-danger btn-small mt-3">Close</a> --}}
                </div>
            </div>
        </div>
    </div>

@endsection
