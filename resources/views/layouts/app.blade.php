<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <meta name ="description", content="@yield('meta_description', 'Power up your Machine Learning initiatives with Aya Data.')">
    <meta name="keywords", content="@yield('meta_keywords', '')">
    <meta name="author" content="RMQ Creative">
    <meta name="dcterms.rights" content="All Rights Reserved. AYA DATA">
    <meta name="copyright" content="All Rights Reserved. AYA DATA" />
    <meta name="application-name" content="Aya Data" />
    <meta name="robots" content="no-follow, no-index">

    <!-- Site Base -->
    {{-- <base href="http://localhost:8000"> --}}

<!-- CSS
  ================================================== -->
  <!-- Themefisher Icon font -->
  <link rel="stylesheet" href="{{asset('plugins/themefisher-font/style.css')}}">
  <!-- bootstrap.min css -->
  <link rel="stylesheet" href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}">
  <!-- Lightbox.min css -->
  <link rel="stylesheet" href="{{asset('plugins/lightbox2/dist/css/lightbox.min.css')}}">
  <!-- animation css -->
  <link rel="stylesheet" href="{{asset('plugins/animate/animate.css')}}">
  <!-- Slick Carousel -->
  <link rel="stylesheet" href="{{asset('plugins/slick/slick.css')}}">
  <!-- Main Stylesheet -->
  <link rel="stylesheet" href="{{asset('css/style.css')}}">

  <!--favicon icon-->
    <link rel="icon" href="{{ asset('img/favicon.png') }}" type="image/png" sizes="16x16" />

</head>

<body id="body">

    <!--
  Start Preloader
  ==================================== -->
  <div id="preloader">
    <div class='preloader'>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>
  <!--
  End Preloader
  ==================================== -->

    {{-- Naigavtion --}}
    @include('inc.navbar')

    {{-- Body content --}}
    <div>
        @yield('content')
    </div>

    {{-- Footer --}}
    @include('inc.footer')

    {{-- cookie warning --}}
    <!-- cookie warning toast -->
    <div class="fixed-bottom">
        <div class="toast w-100 mw-100" role="alert" data-autohide="false" style="background: #0b0b0b; border-radius: 0;">
            <div class="toast-body d-flex flex-column">
                <p class="text-white">
                This website stores data such as cookies to enable site functionality including analytics and personalization. By using this website, you automatically accept that we use cookies.
                </p>
                <div class="">
                    <button type="button" class="btn btn-small btn-outline-light mr-3" id="btnDeny">
                        Deny
                    </button>
                    <button type="button" class="btn btn-small btn-light" id="btnAccept">
                        Accept
                    </button>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal --}}
    {{-- @include('inc.modal') --}}

    <!--
    Essential Scripts
    =====================================-->
    <!-- Main jQuery -->
    <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
    <!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu5nZKbeK-WHQ70oqOWo-_4VmwOwKP9YQ"></script>
    <script  src="{{asset('plugins/google-map/gmap.js')}}"></script>

    <!-- Form Validation -->
    <script src="{{asset('plugins/form-validation/jquery.form.js')}}"></script>
    <script src="{{asset('plugins/form-validation/jquery.validate.min.js')}}"></script>

    <!-- Bootstrap4 -->
    <script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- Parallax -->
    <script src="{{asset('plugins/parallax/jquery.parallax-1.1.3.js')}}"></script>
    <!-- lightbox -->
    <script src="{{asset('plugins/lightbox2/dist/js/lightbox.min.js')}}"></script>
    <!-- Owl Carousel -->
    <script src="{{asset('plugins/slick/slick.min.js')}}"></script>
    <!-- filter -->
    <script src="{{asset('plugins/filterizr/jquery.filterizr.min.js')}}"></script>
    <!-- Smooth Scroll js -->
    <script src="{{asset('plugins/smooth-scroll/smooth-scroll.min.js')}}"></script>
    {{-- fontawersome --}}
    <script src="{{asset('js/all.min.js')}}"></script>
    {{-- TypeIt --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/typed.js/1.1.1/typed.min.js"></script>
    {{-- jquery cookies --}}
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.0/jquery.cookie.min.js"></script>

    <!-- Custom js -->
    <script src="{{asset('js/script.js')}}"></script>

    {{-- SummerNote --}}
    <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'editor' );
    </script>

</body>

</html>
