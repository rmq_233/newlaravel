@extends('layouts.app')

@section('title', 'Join Us | Careers | Aya Data')
@section('meta_description', 'Join Aya Data rapidly growing team and unlock your potential as part of a company that is pushing the boundaries of structured data production in Artificial Intelligence.')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')

<section class="single-page-header" style="background-image: url({{ asset('img/cr-hero.jpg') }})" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="">Careers</h1>
                <p class="text-warning">Join the Best of West Africa's Data Professionals</p>

                @if(Session::get('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
			</div>
		</div>
	</div>
</section>

<section class="service-2 section">
  <div class="container">
    <div class="row">

      <div class="col-12">
        <!-- section title -->
        <div class="title text-center">
            <h3>We're Hiring!</h3>
            <div class="border"></div>
        </div>
        <!-- /section title -->
      </div>

      <div class="col-md-12">
        <div class="row text-center">

            @if(count($posts) > 0)
                @foreach($posts as $careers)

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="service-item">
                        <h5>{{ $careers->title }}</h5>
                        <p>{{ $careers->summary }}</p>
                        <p><small>{{ $careers->created_at->format('j F, Y') }}</small></p>
                        <a href="{{ route('about.careers.open', $careers->slug) }}" class="btn btn-sm btn-dark">View Specifications</a>
                    </div>
                </div><!-- END COL -->
                @endforeach
            @else
                <p>No Opened Positions Yet! Please check back again later. Thank you!</p>
            @endif

        </div>
      </div>
    </div> <!-- End row -->
  </div> <!-- End container -->
</section> <!-- End section -->

@endsection
