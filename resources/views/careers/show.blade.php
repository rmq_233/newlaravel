@extends('layouts.app')

@section('title', 'Join Us | Careers | Aya Data')
@section('meta_description', 'Join Aya Data rapidly growing team and unlock your potential as part of a company that is pushing the boundaries of structured data production in Artificial Intelligence.')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')


<section class="single-page-header" style="background-image: url({{ asset('img/cr-hero.jpg') }})" >
	<div class="row">
        <div class="col-md-12">
            <h2>{{ $post->title }}</h2>
            <p>{{ $post->summary }}</p>
            <p>Published on: {{ $post->created_at->format('j F, Y') }}</p>
            <a href="{{ route('about.careers')}}" class="btn btn-main">Back to Careers</a>
        </div>
    </div>
</section>

<!-- blog details part start -->
<section class="blog-details section">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 offset-md-2">
        <article class="post">
          <!-- Post Content -->
          <div class="post-content">
            <p>{!! $post->body !!}</p>
          </div>
        </article>
      </div>
    </div>
  </div>
</section>
<!-- blog details part end -->

@endsection
