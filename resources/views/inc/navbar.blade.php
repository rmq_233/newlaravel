
<header class="navigation fixed-top">
  <div class="container">
    <!-- main nav -->
    <nav class="navbar navbar-expand-lg navbar-light">
      <!-- logo -->
      <a class="navbar-brand logo" href="/">
        <img class="logo-default" src="{{asset('img/logo.png')}}" alt="logo"/>
        <img class="logo-white" src="{{asset('img/logo.png')}}" alt="logo"/>
      </a>
      <!-- /logo -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
        aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navigation">
        <ul class="navbar-nav ml-auto text-center">
            <li class="nav-item">
            <a class="nav-link" href="{{ route('home') }}">Home</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              Our Company
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{ route('about') }}">About Us</a>
              <a class="dropdown-item" href="{{ route('about.careers') }}">Careers</a>
              <a class="dropdown-item" href="{{ route('about.our-team') }}">Our Team</a>
            </div>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="{{ route('services') }}" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              Services
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{ route('services.computer-vision') }}">Computer Vision</a>
              <a class="dropdown-item" href="{{ route('services.natural-language') }}">Natural Language Processing</a>
              <a class="dropdown-item" href="{{ route('services.data-processing') }}">Data Processing</a>
            </div>
          </li>

          <li class="nav-item ">
            <a class="nav-link" href="{{ route('use-cases') }}">Use Cases</a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="{{ route('our-blog') }}">Blog</a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="{{ route('contact-us') }}">Contact Us</a>
          </li>
        </ul>
      </div>
    </nav>
    <!-- /main nav -->
  </div>
</header>
