<footer id="footer" class="bg-one">
  <div class="top-footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 col-md-3 col-lg-3">
          <ul>
            <li><h3>Our Company</h3></li>
            <li><a href="{{ route('about') }}">About Us</a></li>
            <li><a href="{{ route('about.careers') }}">Careers</a></li>
            <li><a href="{{ route('about.our-team') }}">Our Team</a></li>
            <li><a href="{{ route('our-blog') }}">Blog</a></li>
          </ul>
        </div>
        <!-- End of .col-sm-3 -->

        <div class="col-sm-3 col-md-3 col-lg-3">
          <ul>
            <li><h3>Our Services</h3></li>
            <li><a href="{{ route('services.computer-vision') }}">Computer Vision</a></li>
            <li><a href="{{ route('services.natural-language') }}">Natural Language Processing</a></li>
            <li><a href="{{ route('services.data-processing') }}">Data Processing</a></li>
          </ul>
        </div>
        <!-- End of .col-sm-3 -->

        <div class="col-sm-3 col-md-3 col-lg-3">
          <ul>
            <li><h3 class="">Useful Links</h3></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Security Policy</a></li>
            <li><a href="#">Data Transfer Agreement</a></li>

            @if (isset($LoggedUserInfo) && $LoggedUserInfo)
                <li><a class="text-danger" href="{{ route('auth.logout') }}">Logout</a></li>
            @else
                <li><a href="{{ route('auth.login') }}">Login / Register</a></li>
            @endif

          </ul>
        </div>
        <!-- End of .col-sm-3 -->

        <div class="col-sm-3 col-md-3 col-lg-3">
          <h3>AYA DATA</h3>
            <ul class="text-white">
                <li class="">
                    <i class="tf-ion-ios-home text-primary"></i>
                    <span>15-19 Bloomsbury Way , Holborn, London, WC1A 2TH</span>
                </li>
                <li class="d-flex align-items-center">
                    <span class="text-primary tf-ion-android-phone-portrait mr-2"></span>
                    <a href="tel:+44-33-377-21194">Phone: +44-33-377-21194</a>
                </li>
                <li class="d-flex align-items-center">
                    <span class="text-primary tf-ion-android-mail mr-2"></span>
                    <a href="mailto:info@ayadata.ai">Email: info@ayadata.ai</a>
                </li>
            </ul>
        </div>
        <!-- End of .col-sm-3 -->

      </div>

      <div class="row">
        <div class="social-icon col-md-3 offset-md-5">
            <ul>
                <li><a href="https://www.facebook.com/Ayadatagh/"><i class="tf-ion-social-facebook"></i></a></li>
                <li><a href="https://twitter.com/ayadatagh"><i class="tf-ion-social-twitter"></i></a></li>
                <li><a href="https://www.linkedin.com/company/aya-data"><i class="tf-ion-social-linkedin-outline"></i></a></li>
            </ul>
        </div>
      </div>
    </div> <!-- end container -->
  </div>
  <div class="footer-bottom">
    <h5>Copyright <script>document.write(new Date().getFullYear()); </script> All rights reserved. Aya Data</h5>
  </div>
</footer> <!-- end footer -->
