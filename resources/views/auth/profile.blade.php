@extends('layouts.app')

@section('title', 'Profile | Aya Data')
@section('meta_description', 'Power Up Your Machine Learning Initiatives with Aya Data.')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')

<section class="single-page-header" style="background-image: url({{ asset('img/cases-hero.png')}})" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="">User <span class="text-danger">Profile</span></h1>
                <p class="lead">Welcome {{ $LoggedUserInfo['name'] }}!</p>
                <p class=""><small>{{ $LoggedUserInfo['email'] }}</small></p>

                {{-- admin nav --}}
                <nav class="navbar" style="background: #f1f1f1">
                    <ul class="nav justify-content-center">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Create
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('admin.blog.create') }}">Blog Post</a>
                                <a class="dropdown-item" href="{{ route('admin.cases.create') }}">Use Case</a>
                                <a class="dropdown-item" href="{{ route('admin.careers.create') }}">Job Opening</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="">Settings</a>
                        </li>
                    </ul>

                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="btn btn-small btn-danger" href="{{ route('auth.logout') }}">Logout</a>
                        </li>
                    </ul>
                </nav>
			</div>
		</div>
	</div>
</section>

<section class="portfolio section-sm" id="portfolio">
	<div class="container">
		<div class="row ">
			<div class="col-lg-12">

			</div> <!-- /end col-lg-12 -->
		</div> <!-- end row -->
	</div> <!-- end container -->
</section> <!-- End section -->

@endsection
