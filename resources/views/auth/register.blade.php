@extends('layouts.app')

@section('title', 'Register To App | Aya Data')
@section('meta_description', 'Power Up Your Machine Learning Initiatives with Aya Data.')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')

<section class="single-page-header" style="background-image: url({{ asset('img/cases-hero.png')}})" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="">User <span class="text-primary">Registration</span></h1>
                <p>Register as a staff to update blog and case studies.</p>
			</div>
		</div>
	</div>
</section>

<section class="about-2 section" id="about">
        <div class="container">
            <div class="row">
			<div class="col-md-4 offset-md-4 col-sm-12">
                <form method="post" role="form" action="{{ route('auth.save') }}">
                    @if(Session::get('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                    @endif

                    @if(Session::get('fail'))
                    <div class="alert alert-danger">
                        {{ Session::get('fail') }}
                    </div>
                    @endif

                    @csrf
                    <div class="form-group">
						<input type="text" placeholder="Your Name" class="form-control" name="name" id="name" value="{{ old('name') }}">
						@error('name')
						    <div class="alert alert-danger">{{ $message }}</div>
						@enderror
					</div>
					<div class="form-group">
						<input type="email" placeholder="Your Email" class="form-control" name="email" id="email" value="{{ old('email') }}">
						@error('email')
						    <div class="alert alert-danger">{{ $message }}</div>
						@enderror
					</div>

					<div class="form-group">
						<input type="password" placeholder="Enter Your Password" class="form-control" name="password" id="password">
						@error('password')
						    <div class="alert alert-danger">{{ $message }}</div>
						@enderror
					</div>
                    <div class="form-group">
						<input type="password" placeholder="Confirm Your Password" class="form-control" name="repassword">
						@error('password')
						    <div class="alert alert-danger">{{ $message }}</div>
						@enderror
					</div>

                    <div class="form-group ">
                        <button type="submit" class="btn btn-main text-white btn-block">Register</button>
                        <a class="btn btn-main-alt btn-block" href="{{ route('auth.login')}}">Login</a>
                    </div>
                </form>
			</div> <!-- /end col-lg-12 -->
		</div> <!-- end row -->
	</div> <!-- end container -->
</section> <!-- End section -->

@endsection
