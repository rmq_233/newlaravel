@extends('layouts.app')

@section('title', 'Login To App | Aya Data')
@section('meta_description', 'Power Up Your Machine Learning Initiatives with Aya Data.')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')

<section class="single-page-header" style="background-image: url({{ asset('img/cases-hero.png')}})" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="">User <span class="text-danger">Login</span></h1>
                <p>Login to create, edit or delete a blog post, case study or job opening.</p>
			</div>
		</div>
	</div>
</section>

<section class="about-2 section" id="about">
        <div class="container">
            <div class="row">
			<div class="col-md-4 offset-md-4 col-sm-12">
                <form method="post" role="form" action="{{ route('auth.check') }}">
                    @if(Session::get('fail'))
                    <div class="alert alert-danger">
                        {{ Session::get('fail') }}
                    </div>
                    @endif

                    @if(Session::get('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                    @endif

                    @csrf
					<div class="form-group">
						<input type="email" placeholder="Your Email" class="form-control" name="email" id="email">
						@error('email')
						    <div class="alert alert-danger">{{ $message }}</div>
						@enderror
					</div>

					<div class="form-group">
						<input type="password" placeholder="Password" class="form-control" name="password" id="password">
						@error('password')
						    <div class="alert alert-danger">{{ $message }}</div>
						@enderror
					</div>
                    <div class="form-group ">
                        <button type="submit" id="contact-submit" class="btn btn-main btn-block">Login</button>
                        <a class="btn btn-main-alt btn-block" href="{{ route('auth.register')}}">Register</a>
                    </div>
                </form>
			</div> <!-- /end col-lg-12 -->
		</div> <!-- end row -->
	</div> <!-- end container -->
</section> <!-- End section -->

@endsection
