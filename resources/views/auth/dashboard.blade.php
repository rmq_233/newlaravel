@extends('layouts.app')

@section('title', 'Dashboard | Aya Data')
@section('meta_description', 'Power Up Your Machine Learning Initiatives with Aya Data.')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')

<section class="single-page-header" style="background-image: url({{ asset('img/cases-hero.png')}})" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="">User <span class="text-danger">Dashboard</span></h1>
                <p class="lead">Welcome {{ $LoggedUserInfo['name'] }}!</p>
                <p class=""><small>{{ $LoggedUserInfo['email'] }}</small></p>

                @if(Session::get('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ Session::get('success') }}
                    </div>
                @endif

                {{-- admin nav --}}
                <nav class="navbar" style="background: #f1f1f1">
                    <ul class="nav justify-content-center">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin.dashboard') }}">Dashboard</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Create
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('admin.blog.create') }}">Blog Post</a>
                                <a class="dropdown-item" href="{{ route('admin.cases.create') }}">Use Case</a>
                                <a class="dropdown-item" href="{{ route('admin.careers.create') }}">Job Opening</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Settings</a>
                        </li>
                    </ul>

                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="btn btn-small btn-danger" href="{{ route('auth.logout') }}">Logout</a>
                        </li>
                    </ul>
                </nav>

			</div>
		</div>
	</div>
</section>

<section class="about-2 section" id="about">
    <div class="container">
        <div class="row">
            <ul class="nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-blogs-tab" data-toggle="pill" href="#pills-blogs" role="tab" aria-controls="pills-blogs" aria-selected="true">Blog Posts</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-cases-tab" data-toggle="pill" href="#pills-cases" role="tab" aria-controls="pills-cases" aria-selected="false">Use Cases</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-jobs-tab" data-toggle="pill" href="#pills-jobs" role="tab" aria-controls="pills-jobs" aria-selected="false">Job Openings</a>
                </li>
            </ul>
            <div class="hr"></div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="tab-content" id="pills-tabContent">

                {{-- Blogs --}}
                <div class="tab-pane fade show active" id="pills-blogs" role="tabpanel" aria-labelledby="pills-blogs-tab">
                    <div class="row">
					@if( count($LoggedUserInfo->articles) > 0)
                        @foreach($LoggedUserInfo->articles as $blogpost)
						<div class="col-md-3 col-sm-6 col-xs-6 filtr-item " data-category="mix, blog">
							<div class="post-item">
                                <div class="media-wrapper">
                                    <img src="{{ asset('img/blog/'.$blogpost->image) }}" alt="{{ $blogpost->title }}" class="img-fluid">
                                </div>

                                <div class="content">
                                    <h6><a href="{{ route('blogsingle', $blogpost->slug) }}">{{ $blogpost->title }}</a></h6>
                                    <p>{{ $blogpost->summary }}</p>
                                    <p><small>Written on {{ $blogpost->created_at->format('j F, Y') }}</small></p>
                                    <div class="btn-group" role="group">
                                        <a class="btn btn-light" href="{{ route('blogsingle', $blogpost->slug) }}">View</a>
                                        <a class="btn btn-primary" href="{{ route('admin.blog.edit', [ 'id'=>$blogpost->id ]) }}">Edit</a>
                                        <form method="post" action="{{ route('admin.blog.delete', [ 'id'=>$blogpost->id ]) }}">
                                            @csrf
                                            <button class="btn btn-danger" style="border-radius: 0px !important;" onClick="return confirm('Are you sure?')">Delete</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
						</div>
						@endforeach
					@else
						<p>No Posts Found!</p>
					@endif
                    </div>
                </div>

                {{-- Cases --}}
                <div class="tab-pane fade" id="pills-cases" role="tabpanel" aria-labelledby="pills-cases-tab">
                    <div class="row">
                        @if( count($LoggedUserInfo->cases) > 0)
                        @foreach($LoggedUserInfo->cases as $cases)
                            <div class="col-md-3 col-sm-6 col-xs-6 filtr-item " data-category="mix, {{ $cases->category }}">
                                <div class="post-item mb-3">
                                    <div class="media-wrapper">
                                        <a href="{{ route('cases', $cases->slug) }}">
                                            <img src="{{ asset('img/cases/'.$cases->image) }}" alt="{{ $cases->title }}" class="img-fluid">
                                        </a>
                                    </div>

                                    <div class="content">
                                        <h6>{{ $cases->title }}</h6>
                                        <p>{{ $cases->summary }}</p>
                                        <p><small>Compiled on {{ $cases->created_at->format('j F, Y') }}</small></p>
                                        <div class="btn-group" role="group">
                                            <a class="btn btn-light" href="{{ route('cases', $cases->slug) }}">View</a>
                                            <a class="btn btn-primary" href="{{ route('admin.cases.edit', [ 'id'=>$cases->id ]) }}">Edit</a>
                                            <form method="post" action="{{ route('admin.cases.delete', [ 'id'=>$cases->id ]) }}">
                                                @csrf
                                                <button class="btn btn-danger" style="border-radius: 0px !important;" onClick="return confirm('Are you sure?')">Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <p>No Cases Found!</p>
                        @endif
                    </div>
                </div>

                {{-- Job --}}
                <div class="tab-pane fade" id="pills-jobs" role="tabpanel" aria-labelledby="pills-jobs-tab">
                    <div class="row">
                        @if( count($LoggedUserInfo->jobs) > 0)
                        @foreach($LoggedUserInfo->jobs as $job)
                            <div class="col-md-4 col-sm-12 col-xs-12 filtr-item " >
                                <div class="post-item mb-3">
                                    <div class="media-wrapper">
                                        <img src="{{ asset('img/job.png') }}" alt="{{ $job->title }}" class="img-fluid">
                                    </div>

                                    <div class="content">
                                        <h6>{{ $job->title }}</h6>
                                        <p>{{ $job->summary }}</p>
                                        <p><small>Compiled on {{ $job->created_at->format('j F, Y') }}</small></p>

                                        <div class="btn-group" role="group">
                                            <a class="btn btn-light" href="{{ route('about.careers.open', $job->slug) }}">View</a>
                                            <a class="btn btn-primary" href="{{ route('admin.careers.edit', [ 'id'=>$job->id ]) }}">Edit</a>
                                            <form method="post" action="{{ route('admin.careers.delete', [ 'id'=>$job->id ]) }}">
                                                @csrf
                                                <button class="btn btn-danger" style="border-radius: 0px !important;" onClick="return confirm('Are you sure?')">Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <p>No Jobs Found!</p>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection
