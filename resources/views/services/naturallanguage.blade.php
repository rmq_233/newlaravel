@extends('layouts.app')

@section('title', 'Natural Language Processing - Our Services | Aya Data')
@section('meta_description', 'Power Up Your Machine Learning Initiatives with Aya Data.')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')

<section class="single-page-header" style="background-image: url({{asset('img/nl-hero.png')}})" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="">Natural <span class="text-danger">Language</span> Processing</h1>
                <p class="">Optimize your AI’s understanding of sentiment, semantic meaning and language with English and French text and audio data annotations.</p>
			</div>
		</div>
	</div>
</section>

<section class="service-2 section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row text-center">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="service-item">
                    <img src="{{ asset('img/stakeholder_analysis.png') }}" alt="bounding boxes" class="img-fluid">
                    <h4>Named Entity Recognition</h4>
                    <p class="">Identify. Extrapolate.<br> Organise.</p>
                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#NamedEntityRecognition">
                        Read More
                    </button>
                </div>
            </div><!-- END COL -->

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="service-item">
                    <img src="{{ asset('img/analysis.png') }}" alt="polygon annotation" class="img-fluid">
                    <h4>Sentiment & Content Analysis</h4>
                    <p class="">Optimise your machine / human interactions with sentiment and content analysis.</p>
                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#SentimentContentAnalysis">
                        Read More
                    </button>
                </div>
            </div><!-- END COL -->

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="service-item">
                    <img src="{{ asset('img/foreign_language.png') }}" alt="semantic segmentation" class="img-fluid">
                    <h4>Audio & Text Transcription</h4>
                    <p class="">Make your content go further with audio and text transcription.</p>
                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#AudioTextTranscription">
                        Read More
                    </button>
                </div>
            </div><!-- END COL -->
        </div>
      </div>
    </div> <!-- End row -->
  </div> <!-- End container -->
</section>

{{-- Modals --}}
<div id="NamedEntityRecognition" class="modal fade">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h5 class="modal-title text-capitalize">Natural Language Process - Named Entity Recognition</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <img class="card-img-top mb-3" src="{{ asset('img/name-entity.png') }}" alt="Named Entity Recognition">
                    </div>
                    <div class="col-md-12 text-center">
                        <h5>Identify. Extrapolate. Organise.</h5>
                        <p class="">Understanding language begins with identifying and categorising specific tokens within unstructured text.</p>
                        <p class="">Through Named Entity Recognition (NER), a natural language processing (NLP) method, machines can automatically recognise and predict named entities in text and speech, according to predefined data categories. Sample entities may include names, locations, businesses, objects, quantities or percentages.</p>
                        <p class="">Through the combination of NER, NLP and machine learning, your AI can intelligently extract meaning from speech and text through an evolving understanding of language rules and structure.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer bg-dark">
                <a href="{{ route('contact-us') }}" class="btn btn-main">Book Your Free Consultation</a>
            </div>
        </div>
    </div>
</div>

<div id="SentimentContentAnalysis" class="modal fade">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h5 class="modal-title text-capitalize">Natural Language Process - Sentiment & Content Analysis</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <img class="card-img-top mb-3" src="{{ asset('img/sentiment.png') }}" alt="Sentiment & Content Analysis">
                    </div>
                    <div class="col-md-12 text-center">
                        <h5>Optimise Your Customer Relations with AI-generated Analysis</h5>
                        <p class="">Sentiment analysis uses natural language processing (NLP) techniques to recognise and classify emotions (positive, negative and neutral) in text and speech data. As your machine continuously learns to identify user sentiment towards your presence online, you can make evolving decisions for your brand, product development and customer engagement based on updated and better-structured data sets.</p>
                        <p class="">Content analysis processes text and audio-based messages into actionable, structured data sets. By assessing messages’ attributes through systematic, quantitative and objective, techniques, AI learns to perform deep analysis and labelling of their contents.</p>
                        <p class="">Text-based messages may include published articles, news headlines, social media posts and blog commentary, while audio includes recorded files and online radio.</p>
                        <p>As in real life, when you engage online, your understanding of your customers’ feelings and desires defines your reputation, trustworthiness and ability to satisfy them. Let our experts ensure your AI deeply analyses and prioritises your relationship as much as you do.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer bg-dark">
                <a href="{{ route('contact-us') }}" class="btn btn-main">Book Your Free Consultation</a>
            </div>
        </div>
    </div>
</div>

<div id="AudioTextTranscription" class="modal fade">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h5 class="modal-title text-capitalize">Natural Language Process - Audio & Text Transcription</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <img class="card-img-top mb-3" src="{{ asset('img/translation.jpg') }}" alt="Audio & Text Transcription">
                    </div>
                    <div class="col-md-12 text-center">
                        <h5>Make Your Content Go Further With Audio and Text Transcription</h5>
                        <p class="">Once your AI has optimised its language processing and learnt to analyse, categorise and store data sets based on audio and speech, it can transcribe these files into accurate, shareable text.</p>
                        <p class="">With accurate transcription, users have more control over how they consume your content. They can share soundbites from a podcast as social media messages, or understand what’s spoken in a video, even when the audio quality is inconsistent.</p>
                        <p>Transcription learning software is evolving constantly in sophistication and efficiency. Let’s discuss how to make your speech-based content do more for you.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer bg-dark">
                <a href="{{ route('contact-us') }}" class="btn btn-main">Book Your Free Consultation</a>
            </div>
        </div>
    </div>
</div>

@endsection
