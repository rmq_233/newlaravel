@extends('layouts.app')

@section('title', 'Computer Vision - Our Services | Aya Data')
@section('meta_description', 'Power Up Your Machine Learning Initiatives with Aya Data.')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')

<section class="single-page-header" style="background-image: url({{ asset('img/art-hero.png')}})" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="">Computer <span class="text-warning">Vision</span></h1>
                <p class="">Annotate Video, Image, SAR and LIDAR based.</p>
			</div>
		</div>
	</div>
</section>

<section class="service-2 section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row text-center">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="service-item">
                    <img src="{{ asset('img/dimension.png') }}" alt="bounding boxes" class="img-fluid">
                    <h4>Bounding Boxes</h4>
                    <p class="">Optimise 2D Detection And Labelling With Bounding Boxes.</p>
                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#BoundingBoxes">
                        Read More
                    </button>
                </div>
            </div><!-- END COL -->

            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="service-item">
                    <img src="{{ asset('img/polygon.png') }}" alt="polygon annotation" class="img-fluid">
                    <h4>Polygon Annotation</h4>
                    <p class="">Increase AI Accuracy Through Precision Object Identification.</p>
                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#PolygonAnnotation">
                        Read More
                    </button>
                </div>
            </div><!-- END COL -->

            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="service-item">
                    <img src="{{ asset('img/protocol2.png') }}" alt="semantic segmentation" class="img-fluid">
                    <h4>Semantic Segmentation</h4>
                    <p class="">Categorise Image Elements, Pixel By Pixel.</p>
                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#SemanticSegmentation">
                        Read More
                    </button>
                </div>
            </div><!-- END COL -->

            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="service-item">
                    <img src="{{ asset('img/rip_mapping.png') }}" alt="lidar annotation" class="img-fluid">
                    <h4>LIDAR Annotation</h4>
                    <p class="">Machine Training With Laser Clarity.</p>
                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#LIDARAnnotation">
                        Read More
                    </button>
                </div>
            </div><!-- END COL -->
        </div>
      </div>
    </div> <!-- End row -->
  </div> <!-- End container -->
</section>

{{-- modal --}}
<div id="BoundingBoxes" class="modal fade">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h5 class="modal-title text-capitalize">Computer Vision - Bounding Boxes</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <img class="card-img-top mb-3" src="{{ asset('img/boxes.jpg') }}" alt="Bounding Boxes">
                    </div>
                    <div class="col-md-12 text-center">
                        <h5>Optimise 2D Object Tracking With Bounding Boxes</h5>
                        <p class="">In 2D images, bounding boxes are drawn around targeted objects for positive identification and tracking.</p>
                        <p class=""> Bounding boxes support multiple image targets and label attributions for annotation. These contribute significantly to the development of AI object identification techniques and situational responses.</p>
                        <p class="">Aya Data’s bounding boxes supply you with fully labelled and annotated images, ensuring your AI learning stays the course.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer bg-dark">
                <a href="{{ route('contact-us') }}" class="btn btn-main">Book Your Free Consultation</a>
            </div>
        </div>
    </div>
</div>

<div id="PolygonAnnotation" class="modal fade">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h5 class="modal-title text-capitalize">Computer Vision - Polygon Annotation</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <img class="card-img-top mb-3" src="{{ asset('img/polygon.jpg') }}" alt="Polygon Annotation">
                    </div>
                    <div class="col-md-12 text-center">
                        <h5>Increase Ai Accuracy Through Precision Object Identification</h5>
                        <p class="">When precisely defining shapes within an image, polygon annotation allows AI to recognise objects that are otherwise difficult to detect.</p>
                        <p>Polygon annotation surpasses bounding boxes when detecting and segmenting irregular-shaped objects. This is vital in real-world environments, which feature irregular shapes far more than regular ones. Irregularities include multiple unique shapes with greater curves, angles and lines, coarse features such as logos and graphics, and objects from aerial footage.</p>
                        <p>This technique can vary dramatically in the level of detail that it captures - from a polygon with 1000 vertices to one with 4.</p>
                        <p>To ensure that your AI receives the best possible training data, we collaboratively develop labelling requirements before every engagement, matching up accuracy with speed and other key metrics to find the right balance for you.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer bg-dark">
                <a href="{{ route('contact-us') }}" class="btn btn-main">Book Your Free Consultation</a>
            </div>
        </div>
    </div>
</div>

<div id="SemanticSegmentation" class="modal fade">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h5 class="modal-title text-capitalize">Computer Vision - Semantic Segmentation</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <img class="card-img-top mb-3" src="{{ asset('img/semantic_segmentation.png') }}" alt="Semantic Segmentation">
                    </div>
                    <div class="col-md-12 text-center">
                        <h5>Categorise Image Elements, Pixel By Pixel</h5>
                        <p class="">The semantic segmentation process annotates each pixel within an image. This allows for pixel labelling according to a corresponding list of predefined segment classes. The resulting classifications have different uses, depending on the machine learning goals.</p>
                        <p>Each segment label is assigned a class according to the image’s content. An example of this is segmenting an image by people, clothing, objects, buildings and vehicles.</p>
                        <p>Through semantic segmentation, AIs are trained to improve performance in identification and recognition tasks, image retrieval, object detection and computer vision.</p>
                        <p>Find out more about how Aya Data can help you fine-tune your AI development goals with our segmentation tools.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer bg-dark">
                <a href="{{ route('contact-us') }}" class="btn btn-main">Book Your Free Consultation</a>
            </div>
        </div>
    </div>
</div>

<div id="LIDARAnnotation" class="modal fade">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h5 class="modal-title text-capitalize">Computer Vision - LIDAR Annotation</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <img class="card-img-top mb-3" src="{{ asset('img/bg-1024x586.jpg') }}" alt="LIDAR Annotation">
                    </div>
                    <div class="col-md-12 text-center">
                        <h5>Machine Training With Laser Clarity</h5>
                        <p class="">Lidar (Light Detection & Ranging) is data made using lasers, scanners and specialised GPS receivers to calculate distances to a given object.</p>
                        <p>As one of the most time-intensive annotation tasks, Lidar requires dedicated professionals to create, assign and track object labels using highly advanced 3D software and leading-edge data annotation skills. This results in a high-level combination of 3D tracking, object segmentation and real-time data analysis. With this and its ability to accurately process and sort multiple data streams, Lidar is crucial to the functions of autonomous technology, mapping and weather technology, and many others.</p>
                        <p>When working with Aya Data, you’ll have access to a team that understands how Lidar takes your machine learning to the next level.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer bg-dark">
                <a href="{{ route('contact-us') }}" class="btn btn-main">Book Your Free Consultation</a>
            </div>
        </div>
    </div>
</div>

@endsection
