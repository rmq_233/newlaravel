@extends('layouts.app')

@section('title', 'Create Use Cases - Dashboard | Aya Data')
@section('meta_description', 'Power Up Your Machine Learning Initiatives with Aya Data.')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')

<section class="single-page-header" style="background-image: url({{ asset('img/art-hero.png')}})">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
				<h1 class="">Create A<span class="text-danger">Use Case</span> Article</h1>
                <p class="lead">Welcome {{ $LoggedUserInfo['name'] }}!</p>
                <p class=""><small>{{ $LoggedUserInfo['email'] }}</small></p>

                @if(Session::get('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ Session::get('success') }}
                    </div>
                @endif

                {{-- admin nav --}}
                <nav class="navbar" style="background: #f1f1f1">
                    <ul class="nav justify-content-center">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin.dashboard') }}">Dashboard</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Create
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('admin.blog.create') }}">Blog Post</a>
                                <a class="dropdown-item" href="{{ route('admin.cases.create') }}">Use Case</a>
                                <a class="dropdown-item" href="{{ route('admin.careers.create') }}">Job Opening</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Settings</a>
                        </li>
                    </ul>

                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="btn btn-small btn-danger" href="{{ route('auth.logout') }}">Logout</a>
                        </li>
                    </ul>
                </nav>

			</div>
        </div>
    </div>
</section>

<section class="portfolio section-sm" id="portfolio">
    <div class="container">
        <div class="row ">
            <div class="col-lg-12">
                <form method="post" role="form" action="{{ route('admin.cases.store') }}" enctype="multipart/form-data">
                    @csrf
					<div class="form-group">
						<input type="text" placeholder="Title" class="form-control" name="title">
						@error('title')
						    <div class="alert alert-danger">{{ $message }}</div>
						@enderror
					</div>


                    <div class="form-group">
						<input type="text" placeholder="Summary" class="form-control" name="summary" >
						@error('summary')
						    <div class="alert alert-danger">{{ $message }}</div>
						@enderror
					</div>

                    <div class="form-group">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelectCategory">Category</label>
                            </div>
                            <select type="text" class="custom-select" id="inputGroupSelectCategory" name="category" placeholder="Category">
                                <option value="...">Select...</option>
                                <option value="category1">AgTech</option>
                                <option value="category2">Consumer Insights</option>
                                <option value="category3">Autonomous Vehicles</option>
                            </select>
                        </div>
						{{-- <input type="text" placeholder="Category" class="form-control" name="category" > --}}
						@error('category')
						    <div class="alert alert-danger">{{ $message }}</div>
						@enderror
					</div>

					<div class="form-group">
						<input type="file" placeholder="Image Link" class="form-control" name="cover" >
						@error('cover')
						    <div class="alert alert-danger">{{ $message }}</div>
						@enderror
					</div>


					<div class="form-group">
						<textarea id="editor" rows="6" placeholder="Type Content" class="form-control" name="body"></textarea>
						@error('body')
						    <div class="alert alert-danger">{{ $message }}</div>
						@enderror
					</div>

					<div id="cf-submit">
						<div id="cf-submit">
                        <button type="submit" id="contact-submit" class="btn btn-main">Post Case</button>
                        <button type="reset" class="btn btn-main-alt">Reset</button>
					</div>
					</div>
                </form>
            </div> <!-- /end col-lg-12 -->
        </div> <!-- end row -->
    </div> <!-- end container -->
</section> <!-- End section -->

@endsection
