@extends('layouts.app')

@section('title', 'Use Cases | Aya Data')
@section('meta_description', 'See some of the things we have been working on')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')

<section class="single-page-header" style="background-image: url({{ asset('img/cases-hero.png')}})" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="">Explore Use <span class="text-danger">Cases</span></h1>
                <p class="">See some of the things we've been working on</p>

                @if(Session::get('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
			</div>
		</div>
	</div>
</section>

<section class="portfolio section-sm" id="portfolio">
	<div class="container">
		<div class="row ">
			<div class="col-lg-12">

				<div class="portfolio-filter">
					<button type="button" data-filter="all">All</button>
					<button type="button" data-filter="AgTech">AgTech</button>
					<button type="button" data-filter="Consumer-Insights">Consumer Insights</button>
					<button type="button" data-filter="Autonomous-Vehicles">Autonomous Vehicles</button>
				</div>

				<div class="row filtr-container">
					@if(count($posts) > 0)
						@foreach($posts as $cases)
						<div class="col-md-3 col-sm-6 col-xs-6 filtr-item mb-3" data-category="mix, {{ $cases->category }}">
							<div class="post-item">
								<div class="media-wrapper">
                                    <a href="{{ route('cases', $cases->slug) }}">
                                        <img src="{{ asset('img/cases/'.$cases->image) }}" alt="{{ $cases->title }}" class="img-fluid">
                                    </a>
								</div>

								<div class="content">
									<h5><a href="{{ route('cases', $cases->slug) }}">{{ $cases->title }}</a></h5>
									<p>{{ $cases->summary }}</p>
                                    <p><span class="badge badge-primary">{{ $cases->category }}</span></p>
									<p><small>Compiled on {{ $cases->created_at->format('j F, Y') }}</small></p>
								</div>
							</div>
						</div>
						@endforeach
					@else
						<p>No Cases Found!</p>
					@endif
				</div>
			</div> <!-- /end col-lg-12 -->
		</div> <!-- end row -->
	</div> <!-- end container -->
</section> <!-- End section -->

@endsection
