@extends('layouts.app')

@section('title', 'Use Cases | Aya Data')
@section('meta_description', 'See some of the things we have been working on')
@section('meta_keywords', 'Power ,Machine Learning, initiatives, Aya Data.')

@section('content')


<section class="single-page-header" style="background-image: url({{ asset('img/cr-hero.jpg') }})" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>{{ $post->title }}</h2>
                <p>Compiled on: {{ $post->created_at->format('j F, Y') }}</p>
                <a href="{{ route('use-cases')}}" class="btn btn-main">Back to Use-Cases</a>
			</div>
		</div>
	</div>
</section>

<!-- blog details part start -->
<section class="blog-details section">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 offset-md-2">
        <article class="post">
          <div class="post-image">
            <img src="{{ asset('img/cases/'.$post->image) }}" alt="{{ $post->title }}" class="img-fluid mb-3">
          </div>
          <!-- Post Content -->
          <div class="post-content">

            <p>{!! $post->body !!}</p>
          </div>
        </article>
      </div>
    </div>
  </div>
</section>
<!-- blog details part end -->

@endsection
