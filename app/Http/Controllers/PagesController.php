<?php

namespace App\Http\Controllers;

use App\Mail\ContactFormSubmission;
use App\Models\BlogPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PagesController extends Controller
{

    function index()
    {
        $posts = BlogPost::orderBy('created_at', 'desc')->take(3)->get();
        return view('pages.index', ['posts'=>$posts]);
    }

    function about()
    {
        return view('pages.about');
    }

    function careers()
    {
        return view('pages.careers');
    }

    function team()
    {
        return view('pages.team');
    }

    function services()
    {
        return view('pages.services');
    }

    function computervision()
    {
        return view('pages.computervision');
    }

    function naturallanguage()
    {
        return view('pages.naturallanguage');
    }

    function dataprocessing()
    {
        return view('pages.dataprocessing');
    }

    function contact()
    {
        return view('pages.contact');
    }

    function sendContactUsMessage(Request $request)
    {
        // Validation
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required',
        ]);

        if($this->isOnline()){
            $mail_data = [
              'recepient'=>'james@ayadata.ai',
              'fromEmail'=>$request->email,
              'fromName'=>$request->name,
              'subject'=>$request->subject,
              'body'=>$request->message
            ];

            Mail::send('email.email-template',$mail_data, function($message) use ($mail_data){
                $message->to($mail_data['recepient'])
                        ->from($mail_data['fromEmail'],$mail_data['fromName'])
                        ->subject($mail_data['subject']);
            });

            return redirect()->back()->with('success','Email Sent! We will get back to you in 24hrs.');

        } else {
            return redirect()->back()->withInput()->with('error','No internet connection found. Please try again!');
        }

    }

    function isOnline()
    {
        return true;
    }

    function pagenotfound()
    {
        return view('errors.pagenotfound');
    }

}