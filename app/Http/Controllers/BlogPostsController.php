<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\BlogPost;
use App\Models\Admin;
use View;

class BlogPostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $posts = BlogPost::orderBy('created_at', 'desc')->get();
            return view('posts.blog', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = ['LoggedUserInfo' => Admin::where('id', '=', session('LoggedUser'))->first()];
        return view('posts.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate Posts Params
        $request->validate([
            'title' => 'required',
            'summary' => 'required',
            'cover' => 'required',
            'body' => 'required',
        ]);

        // Blog Img upload location
        $file = $request->file('cover');
        $fileName = bin2hex(random_bytes(8))  . time() . "." . $file->getClientOriginalExtension();

        //move file
        $destinationPath = './img/blog';

        $file->move($destinationPath, $fileName);

        //Create Post
        $post = new BlogPost;
        $post->title = $request->input('title');
        $post->slug = Str::slug($request->input('title'));
        $post->summary = $request->input('summary');
        $post->image = $fileName;
        $post->body = $request->input('body');
        $post->admin_id = $request->session()->get('LoggedUser');
        $post->save();

        return redirect()->route('admin.dashboard')->with('success', 'Great! Your Blog Post Is Added!');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = BlogPost::where('slug', $slug)->first();
        return view('posts.show', ['post' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(BlogPost $post)
    {

        $data = ['LoggedUserInfo' => Admin::where('id', '=', session('LoggedUser'))->first()];
        return view('posts.edit', $data, ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate Posts Params
        $request->validate([
            'title' => 'required',
            'summary' => 'required',
            'cover' => 'required',
            'body' => 'required',
        ]);

        // Blog Img upload location
        $file = $request->file('cover');
        $fileName = bin2hex(random_bytes(8))  . time() . "." . $file->getClientOriginalExtension();

        //move file
        $destinationPath = './img/blog';

        $file->move($destinationPath, $fileName);

        //Create Post
        $post = BlogPost::find($id);
        $post->title = $request->input('title');
        $post->slug = Str::slug($request->input('title'));
        $post->summary = $request->input('summary');
        $post->image = $fileName;
        $post->body = $request->input('body');
        $post->admin_id = $request->session()->get('LoggedUser');
        $post->save();

        return redirect()->route('admin.dashboard')->with('success', 'Great! Your Blog Post Is Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = BlogPost::where('id', $id)->delete();
        return redirect()->route('admin.dashboard')->with('success', 'Great! Your Blog Post Is Deleted!');
    }
}