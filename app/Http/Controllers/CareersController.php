<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Careers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CareersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Careers::orderBy('created_at', 'desc')->get();
        return view('careers.careers', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = ['LoggedUserInfo' => Admin::where('id', '=', session('LoggedUser'))->first()];
        return view('careers.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     */
    public function store(Request $request)
    {
        // Validate Posts Params
        $request->validate([
            'title' => 'required',
            'summary' => 'required',
            'body' => 'required',
        ]);

        //Create Post
        $post = new Careers;
        $post->title = $request->input('title');
        $post->slug = Str::slug($request->input('title'));
        $post->summary = $request->input('summary');
        $post->body = $request->input('body');
        $post->admin_id = $request->session()->get('LoggedUser');
        $post->save();

        return redirect()->route('admin.dashboard')->with('success', 'Great! A New Job Opening Is Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Careers::where('slug', $slug)->first();
        return view('careers.show', ['post' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Careers $post)
    {
        $data = ['LoggedUserInfo' => Admin::where('id', '=', session('LoggedUser'))->first()];
        return view('cases.edit', $data, ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate Posts Params
        $request->validate([
            'title' => 'required',
            'summary' => 'required',
            'body' => 'required',
        ]);

        //Create Post
        $post = new Careers;
        $post->title = $request->input('title');
        $post->slug = Str::slug($request->input('title'));
        $post->summary = $request->input('summary');
        $post->body = $request->input('body');
        $post->admin_id = $request->session()->get('LoggedUser');
        $post->save();

        return redirect()->route('admin.dashboard')->with('success', 'Great! A New Job Opening Is Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Careers::where('id', $id)->delete();
        return redirect()->route('admin.dashboard')->with('success', 'Great! Your Job Opening Is Deleted!');
    }
}