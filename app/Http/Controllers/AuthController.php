<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    function login()
    {
        return view('auth.login');
    }

    function register()
    {
        return view('auth.register');
    }

    function save(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:admins',
            'password' => 'required|min:6|max:12',
            'repassword' => 'required|min:6|max:12',
        ]);

        if( 0 !== strcmp($request->password, $request->repassword) ){
            return back()->with('fail', 'The two passwords do not match. Try again.');
        }

        // Insert into DB
        try {

            $saved = Admin::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);

            if ( $saved ) {
                return back()->with('success', 'New user has been registered!');
            }

        } catch (Illuminate\Database\QueryException $e) {
            return back()->with('fail', 'Oops! Something went wrong, try again later. '. $e->getMessage());
        }

    }

    function check(Request $request)
    {
        // validate requests
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6|max:12',
        ]);

        $userInfo = Admin::where('email', '=', $request->email)->first();

        if (!$userInfo) {
            return back()->with('fail', 'Your email is incorrect. Please try again.');
        } else {
            // Check Password
            if (Hash::check($request->password, $userInfo->password)) {
                $request->session()->put('LoggedUser', $userInfo->id);
                return redirect('admin/dashboard')->with('success','You are now logged in.');
            } else {
                return back()->with('fail', 'Password Incorrect. Please try again.');
            }
        }
    }

    function logout()
    {
        if(session()->has('LoggedUser')){
            session()->pull('LoggedUser');
            return redirect('/auth/login')->with('success', 'You have successfully logged out.');
        }
    }

    function dashboard()
    {
        $data = ['LoggedUserInfo'=>Admin::where('id','=', session('LoggedUser'))->first()];
        return view('auth.dashboard', $data);
    }

    function profile()
    {
        $data = ['LoggedUserInfo' => Admin::where('id', '=', session('LoggedUser'))->first()];
        return view('auth.profile', $data);
    }

    function create()
    {
        $data = ['LoggedUserInfo' => Admin::where('id', '=', session('LoggedUser'))->first()];
        return view('auth.create', $data);
    }


    function settings()
    {
        $data = ['LoggedUserInfo' => Admin::where('id', '=', session('LoggedUser'))->first()];
        return view('auth.settings', $data);
    }
}