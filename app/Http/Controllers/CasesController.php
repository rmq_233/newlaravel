<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cases;
use App\Models\Admin;
use Illuminate\Support\Str;

class CasesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Cases::orderBy('created_at', 'desc')->get();
        return view('cases.cases', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = ['LoggedUserInfo' => Admin::where('id', '=', session('LoggedUser'))->first()];
        return view('cases.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate Posts Params
        $request->validate([
            'title' => 'required',
            'summary' => 'required',
            'cover' => 'required',
            'body' => 'required',
        ]);

        // Blog Img upload location
        $file = $request->file('cover');
        $fileName = bin2hex(random_bytes(8))  . time() . "." . $file->getClientOriginalExtension();

        //move file
        $destinationPath = './img/cases';

        $file->move($destinationPath, $fileName);

        //Create Post
        $post = new Cases;
        $post->title = $request->input('title');
        $post->slug = Str::slug($request->input('title'));
        $post->summary = $request->input('summary');
        $post->category = $request->input('category');
        $post->image = $fileName;
        $post->body = $request->input('body');
        $post->admin_id = $request->session()->get('LoggedUser');
        $post->save();

        return redirect()->route('admin.dashboard')->with('success', 'Great! A New Use-Case Is Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Cases::where('slug', $slug)->first();
        return view('cases.show', ['post' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Cases $post)
    {
        $data = ['LoggedUserInfo' => Admin::where('id', '=', session('LoggedUser'))->first()];
        return view('cases.edit', $data, ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Blog Img upload location
        $file = $request->file('cover');
        $fileName = bin2hex(random_bytes(8))  . time() . "." . $file->getClientOriginalExtension();

        //move file
        $destinationPath = './img/cases';

        $file->move($destinationPath, $fileName);

        //Create Post
        $post = new Cases;
        $post->title = $request->input('title');
        $post->slug = Str::slug($request->input('title'));
        $post->summary = $request->input('summary');
        $post->category = $request->input('category');
        $post->image = $fileName;
        $post->body = $request->input('body');
        $post->admin_id = $request->session()->get('LoggedUser');
        $post->save();

        return redirect()->route('admin.dashboard')->with('success', 'Great! Your Use-Case Is Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Cases::where('id', $id)->delete();
        return redirect()->route('admin.dashboard')->with('success', 'Great! Your Use-Case Is Deleted!');
    }
}