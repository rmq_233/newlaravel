<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function articles()
    {
        return $this->hasMany(BlogPost::class);
    }

    public function cases()
    {
        return $this->hasMany(Cases::class);
    }

    public function jobs()
    {
        return $this->hasMany(Careers::class);
    }
}