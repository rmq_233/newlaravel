(function ($) {
	("use strict");

	/* ========================================================================= */
	/*	Page Preloader
  /* ========================================================================= */

	// window.load = function () {
	// 	document.getElementById('preloader').style.display = 'none';
	// }

	$(window).on("load", function () {
		$("#preloader").fadeOut("slow", function () {
			$(this).remove();
		});
	});

	//Hero Slider
	$(".hero-slider").slick({
		autoplay: true,
		infinite: true,
		arrows: true,
		prevArrow: "<button type='button' class='prevArrow'></button>",
		nextArrow: "<button type='button' class='nextArrow'></button>",
		dots: false,
		autoplaySpeed: 7000,
		pauseOnFocus: false,
		pauseOnHover: false,
	});
	$(".hero-slider").slickAnimation();

	/* ========================================================================= */
	/*	Header Scroll Background Change
  /* ========================================================================= */

	$(window).scroll(function () {
		var scroll = $(window).scrollTop();
		//console.log(scroll);
		if (scroll > 200) {
			//console.log('a');
			$(".navigation").addClass("sticky-header");
		} else {
			//console.log('a');
			$(".navigation").removeClass("sticky-header");
		}
	});

	/* ========================================================================= */
	/*	Portfolio Filtering Hook
  /* =========================================================================  */

	// filter
	setTimeout(function () {
		var containerEl = document.querySelector(".filtr-container");
		var filterizd;
		if (containerEl) {
			filterizd = $(".filtr-container").filterizr({});
		}
	}, 500);

	/* ========================================================================= */
	/*	Testimonial Carousel
  /* =========================================================================  */

	//Init the slider
	$(".testimonial-slider").slick({
		infinite: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 2000,
	});

	/* ========================================================================= */
	/*	Clients Slider Carousel
  /* =========================================================================  */

	//Init the slider
	$(".clients-logo-slider").slick({
		infinite: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 2000,
		slidesToShow: 4,
		slidesToScroll: 1,
	});

	/* ========================================================================= */
	/*	Company Slider Carousel
  /* =========================================================================  */
	$(".company-gallery").slick({
		infinite: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 2000,
		slidesToShow: 5,
		slidesToScroll: 1,
	});

	/* ========================================================================= */
	/*   Contact Form Validating
  /* ========================================================================= */

	/* ========================================================================= */
	/*	On scroll fade/bounce effect
  /* ========================================================================= */
	var scroll = new SmoothScroll('a[href*="#"]');

	// -----------------------------
	//  Count Up
	// -----------------------------
	function counter() {
		if ($(".counter").length !== 0) {
			var oTop = $(".counter").offset().top - window.innerHeight;
		}
		if ($(window).scrollTop() > oTop) {
			$(".counter").each(function () {
				var $this = $(this),
					countTo = $this.attr("data-count");
				$({
					countNum: $this.text(),
				}).animate(
					{
						countNum: countTo,
					},
					{
						duration: 1000,
						easing: "swing",
						step: function () {
							$this.text(Math.floor(this.countNum));
						},
						complete: function () {
							$this.text(this.countNum);
						},
					}
				);
			});
		}
	}
	// -----------------------------
	//  On Scroll
	// -----------------------------
	$(window).scroll(function () {
		counter();
	});

	// ------------------------------
	// Modal & Cookie Law
	// ------------------------------
	// $(document).ready(function () {
	// 	$("#infoModal").modal("show");
	// });
	$(document).ready(function () {
		if ($.cookie("pop") == null) {
			$("#infoModal").modal("show");
			$.cookie("pop", "1");
		}
	});

	function setCookie(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + (value || "") + expires + "; path=/";
	}
	function getCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(";");
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == " ") c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		}
		return null;
	}

	function eraseCookie(name) {
		document.cookie =
			name + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
	}

	function cookieConsent() {
		if (!getCookie("allowCookies")) {
			$(".toast").toast("show");
		}
	}

	$("#btnDeny").click(() => {
		eraseCookie("allowCookies");
		$(".toast").toast("hide");
	});

	$("#btnAccept").click(() => {
		setCookie("allowCookies", "1", 7);
		$(".toast").toast("hide");
	});

	// load
	cookieConsent();

	// ------------------------------
	// Type Effect
	// ------------------------------
	$(".typeIt").typed({
		strings: ["Resourcefulness", "Endurance"],
		typeSpeed: 50,
		backSpeed: 50,
		backDelay: 1500,
		showCursor: true,
		loop: true,
	});

	// ------------------------------
	// Timeline Homepage
	// ------------------------------
    (function () {
			"use strict";

			// define variables
			var items = document.querySelectorAll(".timeline li");

			// check if an element is in viewport
			// http://stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport
			function isElementInViewport(el) {
				var rect = el.getBoundingClientRect();
				return (
					rect.top >= 0 &&
					rect.left >= 0 &&
					rect.bottom <=
						(window.innerHeight || document.documentElement.clientHeight) &&
					rect.right <=
						(window.innerWidth || document.documentElement.clientWidth)
				);
			}

			function callbackFunc() {
				for (var i = 0; i < items.length; i++) {
					if (isElementInViewport(items[i])) {
						items[i].classList.add("in-view");
					}
				}
			}

			// listen for events
			window.addEventListener("load", callbackFunc);
			window.addEventListener("resize", callbackFunc);
			window.addEventListener("scroll", callbackFunc);
		})();


})(jQuery);
