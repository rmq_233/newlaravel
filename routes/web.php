<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\BlogPostsController;
use App\Http\Controllers\CasesController;
use App\Http\Controllers\CareersController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ComputerVisionController;
use App\Http\Controllers\NaturalLanguageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/',[PagesController::class, 'index'])->name('home');

Route::get('/about', [PagesController:: class, 'about'])->name('about');
Route::get('/about/team', [PagesController::class, 'team'])->name('about.our-team');

Route::get('/about/careers', [CareersController::class, 'index'])->name('about.careers');
Route::get('/about/careers/{slug}', [CareersController::class, 'show'])->name('about.careers.open');

// Services
Route::get('/services', [PagesController::class, 'services'])->name('services');

// Computer Vision
Route::get('/services/computervision', [ComputerVisionController::class, 'index'])->name('services.computer-vision');

// Natural Language
Route::get('/services/naturallanguage', [NaturalLanguageController::class, 'index'])->name('services.natural-language');

// Data Processing
Route::get('/services/dataprocessing', [PagesController::class, 'dataprocessing'])->name('services.data-processing');

// Use Cases
Route::get('/cases', [ CasesController::class, 'index'])->name('use-cases');
Route::get('/cases/{slug}', [CasesController::class, 'show'])->name('cases');

// Our Blog
Route::get('/blog', [BlogPostsController::class, 'index'])->name('our-blog');
Route::get('/blog/{slug}', [BlogPostsController::class, 'show'])->name('blogsingle');

// Contact Us
Route::get('/contact', [PagesController::class, 'contact'])->name('contact-us');
Route::post('/send', [PagesController::class, 'sendContactUsMessage'])->name('send.email');

// Authentication
Route::post('/auth/save', [AuthController::class, 'save'])->name('auth.save');
Route::post('/auth/check', [AuthController::class, 'check'])->name('auth.check');
Route::get('/auth/logout', [AuthController::class, 'logout'])->name('auth.logout');

//Authorized Pages Only
Route::group(['middleware'=>['AuthCheck']], function(){
    // Admin
    Route::get('/auth/login', [AuthController::class, 'login'])->name('auth.login');
    Route::get('/auth/register', [AuthController::class, 'register'])->name('auth.register');
    Route::get('/admin/dashboard', [AuthController::class, 'dashboard'])->name('admin.dashboard');
    Route::get('/admin/profile', [AuthController::class, 'profile'])->name('admin.profile');
    Route::get('/admin/settings', [AuthController::class, 'settings'])->name('admin.settings');
    Route::post('/admin/store', [BlogPostsController::class, 'store'])->name('admin.store');

    // Blog - CRUD
    Route::get('/admin/blog/create', [BlogPostsController::class, 'create'])->name('admin.blog.create');
    Route::post('/admin/blog/store', [BlogPostsController::class, 'store'])->name('admin.blog.store');
    Route::get('/admin/blog/{post}', [BlogPostsController::class, 'edit'])->name('admin.blog.edit');
    Route::post('/admin/blog/{id}', [BlogPostsController::class, 'update'])->name('admin.blog.edit');
    Route::post('/admin/blog/delete/{id}', [BlogPostsController::class, 'destroy'])->name('admin.blog.delete');

    // Careers - CRUD
    Route::get('/admin/careers/create', [CareersController::class, 'create'])->name('admin.careers.create');
    Route::post('/admin/careers/store', [CareersController::class, 'store'])->name('admin.careers.store');
    Route::get('/admin/careers/{post}', [CareersController::class, 'edit'])->name('admin.careers.edit');
    Route::post('/admin/careers/{id}', [CareersController::class, 'update'])->name('admin.careers.edit');
    Route::post('/admin/careers/delete/{id}', [CareersController::class, 'destroy'])->name('admin.careers.delete');

    // Cases - CRUD
    Route::get('/admin/cases/create', [CasesController::class, 'create'])->name('admin.cases.create');
    Route::post('/admin/cases/store', [CasesController::class, 'store'])->name('admin.cases.store');
    Route::get('/admin/cases/{post}', [CasesController::class, 'edit'])->name('admin.cases.edit');
    Route::post('/admin/cases/{id}', [CasesController::class, 'update'])->name('admin.cases.edit');
    Route::post('/admin/cases/delete/{id}', [CasesController::class, 'destroy'])->name('admin.cases.delete');
});

// 404 Page
Route::get('pagenotfound', [PagesController::class, 'notfound'])->name('pagenotfound');